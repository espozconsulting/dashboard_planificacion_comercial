from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin

from .models import *
from .forms import *


class PeriodTypeListView(LoginRequiredMixin, ListView):
    model = PeriodType
    template_name = "app_master/list_period_type.html"


class PeriodTypeCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    model = PeriodType
    form_class = PeriodTypeForm
    template_name = "app_master/edit_period_type.html"
    success_url = "/app_master/period_type_list"
    success_message = "%(period)s ha sido creado satisfactoriamente"


class PeriodTypeUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = PeriodType
    form_class = PeriodTypeForm
    template_name = "app_master/edit_period_type.html"
    success_url = "/app_master/period_type_list"
    success_message = "%(period)s ha sido actualizado satisfactoriamente"


class PeriodListView(LoginRequiredMixin, ListView):
    model = Period
    template_name = "app_master/list_period.html"


class PeriodCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    model = Period
    form_class = PeriodForm
    template_name = "app_master/edit_period.html"
    success_url = "/app_master/period_list"
    success_message = "%(name)s ha sido creado satisfactoriamente"


class PeriodUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Period
    form_class = PeriodForm
    template_name = "app_master/edit_period.html"
    success_url = "/app_master/period_list"
    success_message = "%(name)s ha sido actualizado satisfactoriamente"


class SectorListView(LoginRequiredMixin, ListView):
    model = Sector
    template_name = "app_master/list_sector.html"


class SectorCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    model = Sector
    form_class = SectorForm
    template_name = "app_master/edit_sector.html"
    success_url = "/app_master/sector_list"
    success_message = "%(name)s ha sido creado satisfactoriamente"


class SectorUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Sector
    form_class = SectorForm
    template_name = "app_master/edit_sector.html"
    success_url = "/app_master/sector_list"
    success_message = "%(name)s ha sido actualizado satisfactoriamente"
