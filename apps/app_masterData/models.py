# -*- coding: utf-8 -*-
from django.db import models


class PeriodType(models.Model):
    period = models.CharField(verbose_name="Tipo de periodo", max_length=50)

    def __str__(self):
        return self.period


class Period(models.Model):
    period_type = models.ForeignKey(
        PeriodType, on_delete=models.CASCADE, verbose_name="Tipo de periodo", related_name="period_type")
    name = models.CharField(verbose_name="Nombre", max_length=50)

    def __str__(self):
        return self.name


class Sector(models.Model):
    code = models.CharField(verbose_name="Cód. Sector",
                            max_length=50, primary_key=True)
    name = models.CharField(verbose_name="Sector", max_length=150)
    id = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class Plants(models.Model):
    code = models.CharField(verbose_name="Cód. Planta",
                            max_length=50, primary_key=True)
    name = models.CharField(verbose_name="Planta", max_length=150)

    def __str__(self):
        return self.name


class SegmentacionInsumo(models.Model):
    plant = models.ForeignKey(
        Plants, on_delete=models.CASCADE, verbose_name="Planta")
    hierarchicalLevel = models.CharField(
        verbose_name="NJ Insumo", max_length=50)
    code = models.CharField(verbose_name="Cod. SAP", max_length=50)
    description = models.CharField(verbose_name="Descripción", max_length=250)
    clasification = models.CharField(
        verbose_name="Clasificación", max_length=50)

    def __str__(self):
        return '%s - %s' % (self.plant, self.code)
