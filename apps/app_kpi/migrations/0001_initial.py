# Generated by Django 2.2 on 2019-06-03 05:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('app_masterData', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Chart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='Nombre')),
                ('history_count', models.IntegerField(default=1, verbose_name='Cant. historial')),
                ('title', models.CharField(blank=True, max_length=50, null=True, verbose_name='Título')),
                ('subtitle', models.CharField(blank=True, max_length=50, null=True, verbose_name='Sub título')),
                ('xaxis_title', models.CharField(blank=True, max_length=50, null=True, verbose_name='Título eje horizontal')),
                ('yaxis_title', models.CharField(blank=True, max_length=50, null=True, verbose_name='Título eje vertical')),
                ('suffix', models.CharField(blank=True, max_length=50, null=True, verbose_name='Sufijo')),
                ('show_suffix', models.BooleanField(blank=True, default=False, null=True, verbose_name='Mostrar sufijo')),
            ],
        ),
        migrations.CreateModel(
            name='DashStyle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Indicator',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='Indicador')),
            ],
        ),
        migrations.CreateModel(
            name='MarkerSymbol',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='SizeChart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='TypeChart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='ChartData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('categories', models.CharField(max_length=150, verbose_name='Categorías')),
                ('value', models.FloatField(default=0, verbose_name='Valor')),
                ('serie', models.CharField(max_length=150, verbose_name='Nombre serie')),
                ('filter_type', models.CharField(blank=True, max_length=50, null=True, verbose_name='Tipo de filtro')),
                ('filter_name', models.CharField(blank=True, max_length=150, null=True, verbose_name='Filtro')),
                ('chart', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_kpi.Chart', verbose_name='Gráfico')),
                ('period', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_masterData.Period', verbose_name='Periodo')),
            ],
        ),
        migrations.AddField(
            model_name='chart',
            name='dash_style',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app_kpi.DashStyle', verbose_name='Tipo de línea'),
        ),
        migrations.AddField(
            model_name='chart',
            name='indicator',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_kpi.Indicator', verbose_name='Indicador'),
        ),
        migrations.AddField(
            model_name='chart',
            name='marker_symbol',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app_kpi.MarkerSymbol', verbose_name='Tipo de marcador'),
        ),
        migrations.AddField(
            model_name='chart',
            name='size',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app_kpi.SizeChart', verbose_name='Tamaño de gráfico'),
        ),
        migrations.AddField(
            model_name='chart',
            name='type_chart',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_kpi.TypeChart', verbose_name='Tipo de gráfico'),
        ),
    ]
