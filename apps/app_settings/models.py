# -*- coding: utf-8 -*-
from django.db import models
from apps.app_masterData.models import *


class ImportFile(models.Model):
    name = models.CharField(verbose_name="Nombre", max_length=50)
    first_row = models.IntegerField(
        verbose_name="Primera fila de datos", default=1)
    name_model = models.CharField(
        verbose_name="Modelo", max_length=150, null=True, blank=True)

    def __str__(self):
        return self.name


class Homologation(models.Model):
    import_file = models.ForeignKey(ImportFile, on_delete=models.CASCADE)
    db_header = models.CharField(verbose_name="Destino", max_length=50)
    entry_header = models.IntegerField(verbose_name="Entrada", default=0)

    def __str__(self):
        return '%s' % (self.import_file)
