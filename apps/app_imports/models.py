from django.db import models
from apps.app_masterData.models import *


class ProgramaProduccion(models.Model):
    sector = models.ForeignKey(
        Sector, on_delete=models.CASCADE, verbose_name="Sector")
    period = models.ForeignKey(
        Period, on_delete=models.CASCADE, verbose_name="Periodo")
    center = models.CharField(verbose_name="Centro Productivo", max_length=50)
    date_proccess = models.DateField(verbose_name="Fecha de proceso",
                                     auto_now=False, auto_now_add=False)
    level2 = models.CharField(verbose_name="Nivel 2", max_length=150)
    code = models.CharField(verbose_name="Cod. SAP", max_length=50)
    description = models.CharField(verbose_name="Descripción", max_length=250)
    destiny = models.CharField(verbose_name="Pais", max_length=50)
    kg_original = models.FloatField(verbose_name="KG Plan Original", default=0)
    kg_modified = models.FloatField(
        verbose_name="KG Plan Modificado", default=0)
    kg_real = models.FloatField(verbose_name="KG Real", default=0)

    def __str__(self):
        return '%s - %s' % (self.period, self.sector)


class BDProduccion(models.Model):
    period = models.ForeignKey(
        Period, on_delete=models.CASCADE, verbose_name="Periodo")
    code = models.CharField(verbose_name="Cod. SAP", max_length=50)
    description = models.CharField(verbose_name="Descripción", max_length=250)
    date = models.DateField(verbose_name="Fecha",
                            auto_now=False, auto_now_add=False)
    k_producer = models.FloatField(default=0, verbose_name="Kg a producir")
    k_real = models.FloatField(default=0, verbose_name="Kg reales")
    plant = models.CharField(verbose_name="Planta", max_length=150)
    productive_cap = models.FloatField(
        default=0, verbose_name="Tope Productivo")
    sector = models.ForeignKey(Sector, on_delete=models.CASCADE)

    def __str__(self):
        return '%s - %s, %s - %s' % (self.period, self.sector, self.code, self.description)


class StockHomologado(models.Model):
    period = models.ForeignKey(
        Period, on_delete=models.CASCADE, verbose_name="Periodo")
    sector = models.ForeignKey(
        Sector, on_delete=models.CASCADE, verbose_name="Sector")
    level2 = models.CharField(verbose_name="Nivel 2", max_length=150)
    code = models.CharField(verbose_name="Cod. SAP", max_length=50)
    description = models.CharField(verbose_name="Descripción", max_length=250)
    supplying_center = models.CharField(
        verbose_name="Centro Suministrador", max_length=150)
    center = models.CharField(
        verbose_name="Centro", max_length=150)
    destiny = models.CharField(verbose_name="Pais", max_length=50)
    free_stock = models.FloatField(default=0, verbose_name="Stock Libre")
    blocked_stock = models.FloatField(
        default=0, verbose_name="Stock Bloqueado")

    def __str__(self):
        return '%s - %s, %s - %s' % (self.period, self.sector, self.code, self.description)


class StockMeta(models.Model):
    period = models.ForeignKey(
        Period, on_delete=models.CASCADE, verbose_name="Periodo")
    sector = models.CharField(verbose_name="Sector",
                              max_length=100, default="")
    code = models.CharField(verbose_name="Cod. SAP", max_length=50)
    description = models.CharField(verbose_name="Descripción", max_length=250)
    stock = models.FloatField(default=0, verbose_name="Stock Meta")
    level2 = models.CharField(verbose_name="Nivel 2", max_length=250)

    def __str__(self):
        return '%s - %s, %s - %s' % (self.period, self.sector, self.code, self.description)


class StockInsumos(models.Model):
    period = models.ForeignKey(
        Period, on_delete=models.CASCADE, verbose_name="Periodo")
    center = models.CharField(verbose_name="Centro", max_length=150)
    code = models.CharField(verbose_name="Cod. SAP", max_length=50)
    description = models.CharField(verbose_name="Descripción", max_length=250)
    stock = models.FloatField(default=0, verbose_name="Stock")
    umb = models.CharField(verbose_name="UMB", max_length=50)
    free = models.FloatField(default=0, verbose_name="Valor libre util.")
    almacen = models.CharField(verbose_name="Almacén", max_length=150)
    group = models.CharField(verbose_name="Gpo.artíc.", max_length=150)

    def __str__(self):
        return '%s - %s, %s' % (self.period, self.code, self.description)


class ConsumoEAD(models.Model):
    period = models.ForeignKey(
        Period, on_delete=models.CASCADE, verbose_name="Periodo")
    hour = models.CharField(verbose_name="Hora de entrada", max_length=150)
    reserve = models.CharField(verbose_name="Nro reserva", max_length=150)
    classmov = models.CharField(
        verbose_name="Clase de movimiento", max_length=50)
    document = models.CharField(
        verbose_name="Documento material", max_length=50)
    plant = models.ForeignKey(Plants, on_delete=models.CASCADE)
    name = models.CharField(verbose_name="Nombre 1", max_length=50)
    almacen = models.CharField(verbose_name="Almacén", max_length=50)
    lote = models.CharField(verbose_name="Lote", max_length=50)
    code = models.CharField(verbose_name="Cod. SAP", max_length=50)
    description = models.CharField(verbose_name="Descripción", max_length=250)
    date = models.DateField(verbose_name="Fecha",
                            auto_now=False, auto_now_add=False)
    quantity_um = models.FloatField(
        default=0, verbose_name="Ctd.en UM entrada")
    um = models.CharField(verbose_name="UM", max_length=50)
    quantity = models.FloatField(default=0, verbose_name="Cantidad")
    um_base = models.CharField(verbose_name="UM Base", max_length=50)

    def __str__(self):
        return '%s - %s, %s' % (self.period, self.code, self.description)


class ReunionVolumen(models.Model):
    period = models.ForeignKey(
        Period, on_delete=models.CASCADE, verbose_name="Periodo")
    sector = models.ForeignKey(
        Sector, on_delete=models.CASCADE, verbose_name="Sector")
    code = models.CharField(verbose_name="Cod. SAP", max_length=50)
    description = models.CharField(verbose_name="Descripción", max_length=250)
    destiny = models.CharField(verbose_name="Destino", max_length=50)
    production = models.FloatField(verbose_name="Producción", default=0)

    def __str__(self):
        return '%s - %s, %s' % (self.period, self.code, self.description)
