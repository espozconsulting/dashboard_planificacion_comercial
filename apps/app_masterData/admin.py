from django.contrib import admin
from .models import *


@admin.register(Sector)
class SectorAdmin(admin.ModelAdmin):
    display_list = ('code', 'name',)
