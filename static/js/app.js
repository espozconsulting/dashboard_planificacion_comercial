$(document).ready(function () {

  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i].trim();
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }

  var csrftoken = getCookie('csrftoken');

  function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }

  $.ajaxSetup({
    beforeSend: function (xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
    }
  });

  $("#id_import_file").on("change", function () {
    var id = $(this).val();
    if (id > 0) {
      $.ajax({
        'method': 'POST',
        'url': '/ajax/get_columns/',
        'data': {
          'id': id
        },
        'success': function (res) {
          if (res.response.length > 0) {
            $("#result > table > tbody").empty();
            for (let index = 0; index < res.response.length; index++) {
              const element = res.response[index];
              new_element = "<tr><td>" + element.column + "</td><td><select class='form-control'><option value=0>Sin asignar</option>"
              for (let x = 65; x < 91; x++) {
                new_element = new_element + "<option value=" + x + ">Columna " + String.fromCharCode(x) + "</option>";
              };
              for (let x = 65; x < 91; x++) {
                new_element = new_element + "<option value=" + x + ">Columna A" + String.fromCharCode(x) + "</option>";
              };
              new_element = new_element + "</select></td></tr>";
              $("#result > table > tbody").append(new_element);
            }
          }
        }
      });
    }
  });

  $("#create_homologation").on("click", function () {
    if ($("#id_import_file").val() > 0) {
      var rowCount = $('#result > table > tbody > tr').length;
      var count = 0;
      $("#result > table > tbody > tr").each(function () {
        count += 1;
        var text = $(this).find('td').eq(0).text();
        var value = $(this).find('select').val();
        if (value >= 65) {
          value = value - 65
          $.ajax({
            'method': 'POST',
            'url': '/ajax/save_homologation/',
            'data': {
              'import_file_id': $("#id_import_file").val(),
              'from': value,
              'to': text
            },
            'success': function (res) {
            }
          })
        }
        if (count == rowCount) {
          Swal.fire({
            title: "HOMOLOGACIÓN DE DATOS",
            type: "success",
            html: "<p class='text-center'>La homologación ha sido guardada exitosamente</p>"
          }).then((result) => {
            if (result.value) {
              window.location = '/app_settings/homologation_list'
            }
          })
        }
      });
    }
  });

  $("#id_tipo_periodo").on("change", function () {
    if ($(this).val() != 0) {
      $("#period").removeClass("hidden");
      $.ajax({
        'method': 'POST',
        'url': '/ajax/get_periods/',
        'data': {
          'id': $(this).val()
        },
        'success': function (res) {
          if (res.response.length > 0) {
            var element = $("#periodo_id");
            element.html("");
            element.append("<option value=0>Seleccionar...</option>")
            for (let index = 0; index < res.response.length; index++) {
              const data = res.response[index];
              element.append("<option value=" + data.id + ">" + data.period + "</option>");
            }
          }
        }
      });
    } else {
      $("#period").addClass("hidden");
    }
  });

  $("#confirm_import_production_program").on("click", function () {
    if ($("#id_tipo_periodo").val() != 0) {
      if ($("#id_sector").val() != 0 && $("#periodo_id").val() != 0) {
        $(".spinner-modal").removeClass("hidden");
        setTimeout(function () {
          var table = $('#result_list_ProgramaProduccion').tableToJSON();
          var periodo = $("#periodo_id").val();
          $.ajax({
            'method': 'POST',
            'url': '/ajax/save_import_programa_produccion/',
            'dataType': 'json',
            'data': {
              'period': periodo,
              'table': JSON.stringify(table)
            },
            'success': function (res) {
              if (res.response) {
                $(".spinner-modal").addClass("hidden");
                Swal.fire({
                  title: "IMPORTACIÓN DE DATOS",
                  type: "success",
                  html: "<p class='text-center'>La importación ha sido realizada exitosamente</p>"
                }).then((result) => {
                  if (result.value) {
                    window.location = '/app_imports/production_program_list'
                  }
                })
              }
              if (res.error) {
                $(".spinner-modal").addClass("hidden");
                Swal.fire({
                  title: "IMPORTACIÓN DE DATOS",
                  type: "error",
                  html: "<p class='text-center'>" + res.error + "</p>"
                });
              }
            }
          });
        }, 2000);
      } else {
        Swal.fire({
          title: "IMPORTACIÓN DE DATOS",
          type: "error",
          html: "<p class='text-center'>Faltan datos por seleccionar</p>"
        });
        $(".spinner-modal").addClass("hidden");
      }
    } else {
      Swal.fire({
        title: "IMPORTACIÓN DE DATOS",
        type: "error",
        html: "<p class='text-center'>Faltan datos por seleccionar</p>"
      });
      $(".spinner-modal").addClass("hidden");
    }
  })

  $("#confirm_import_db_production").on("click", function () {
    if ($("#id_tipo_periodo").val() != 0) {
      if ($("#periodo_id").val() != 0) {
        $(".spinner-modal").removeClass("hidden");
        setTimeout(function () {
          var table = $('#result_list_BDProduccion').tableToJSON();
          var periodo = $("#periodo_id").val();
          $.ajax({
            'method': 'POST',
            'url': '/ajax/save_import_bd_produccion/',
            'dataType': 'json',
            'data': {
              'period': periodo,
              'table': JSON.stringify(table)
            },
            'success': function (res) {
              if (res.response) {
                $(".spinner-modal").addClass("hidden");
                Swal.fire({
                  title: "IMPORTACIÓN DE DATOS",
                  type: "success",
                  html: "<p class='text-center'>La importación ha sido realizada exitosamente</p>"
                }).then((result) => {
                  if (result.value) {
                    window.location = '/app_imports/db_production_list'
                  }
                })
              }
              if (res.error) {
                $(".spinner-modal").addClass("hidden");
                Swal.fire({
                  title: "IMPORTACIÓN DE DATOS",
                  type: "error",
                  html: "<p class='text-center'>" + res.error + "</p>"
                });
              }
            }
          });
        }, 2000);
      } else {
        Swal.fire({
          title: "IMPORTACIÓN DE DATOS",
          type: "error",
          html: "<p class='text-center'>Faltan datos por seleccionar</p>"
        });
        $(".spinner-modal").addClass("hidden");
      }
    } else {
      Swal.fire({
        title: "IMPORTACIÓN DE DATOS",
        type: "error",
        html: "<p class='text-center'>Faltan datos por seleccionar</p>"
      });
      $(".spinner-modal").addClass("hidden");
    }
  })

  $("#confirm_import_stock_homologado").on("click", function () {
    if ($("#id_tipo_periodo").val() != 0) {
      if ($("#periodo_id").val() != 0) {
        $(".spinner-modal").removeClass("hidden");
        setTimeout(function () {
          var table = $('#result_list_StockHomologado').tableToJSON();
          var periodo = $("#periodo_id").val();
          $.ajax({
            'method': 'POST',
            'url': '/ajax/save_import_stock_homologado/',
            'dataType': 'json',
            'data': {
              'period': periodo,
              'table': JSON.stringify(table)
            },
            'success': function (res) {
              if (res.response) {
                $(".spinner-modal").addClass("hidden");
                Swal.fire({
                  title: "IMPORTACIÓN DE DATOS",
                  type: "success",
                  html: "<p class='text-center'>La importación ha sido realizada exitosamente</p>"
                }).then((result) => {
                  if (result.value) {
                    window.location = '/app_imports/stock_homologado_list'
                  }
                })
              }
              if (res.error) {
                $(".spinner-modal").addClass("hidden");
                Swal.fire({
                  title: "IMPORTACIÓN DE DATOS",
                  type: "error",
                  html: "<p class='text-center'>" + res.error + "</p>"
                });
              }
            }
          });
        }, 2000);
      } else {
        Swal.fire({
          title: "IMPORTACIÓN DE DATOS",
          type: "error",
          html: "<p class='text-center'>Faltan datos por seleccionar</p>"
        });
        $(".spinner-modal").addClass("hidden");
      }
    } else {
      Swal.fire({
        title: "IMPORTACIÓN DE DATOS",
        type: "error",
        html: "<p class='text-center'>Faltan datos por seleccionar</p>"
      });
      $(".spinner-modal").addClass("hidden");
    }
  })

  $("#confirm_import_stock_meta").on("click", function () {
    if ($("#id_tipo_periodo").val() != 0) {
      if ($("#id_sector").val() != 0 && $("#periodo_id").val() != 0) {
        $(".spinner-modal").removeClass("hidden");
        setTimeout(function () {
          var table = $('#result_list_StockMeta').tableToJSON();
          var periodo = $("#periodo_id").val();
          $.ajax({
            'method': 'POST',
            'url': '/ajax/save_import_stock_meta/',
            'dataType': 'json',
            'data': {
              'period': periodo,
              'table': JSON.stringify(table)
            },
            'success': function (res) {
              if (res.response) {
                $(".spinner-modal").addClass("hidden");
                Swal.fire({
                  title: "IMPORTACIÓN DE DATOS",
                  type: "success",
                  html: "<p class='text-center'>La importación ha sido realizada exitosamente</p>"
                }).then((result) => {
                  if (result.value) {
                    window.location = '/app_imports/stock_meta_list'
                  }
                })
              }
              if (res.error) {
                $(".spinner-modal").addClass("hidden");
                Swal.fire({
                  title: "IMPORTACIÓN DE DATOS",
                  type: "error",
                  html: "<p class='text-center'>" + res.error + "</p>"
                });
              }
            }
          });
        }, 2000);
      } else {
        Swal.fire({
          title: "IMPORTACIÓN DE DATOS",
          type: "error",
          html: "<p class='text-center'>Faltan datos por seleccionar</p>"
        });
        $(".spinner-modal").addClass("hidden");
      }
    } else {
      Swal.fire({
        title: "IMPORTACIÓN DE DATOS",
        type: "error",
        html: "<p class='text-center'>Faltan datos por seleccionar</p>"
      });
      $(".spinner-modal").addClass("hidden");
    }
  })

  $("#confirm_import_stock_insumos").on("click", function () {
    if ($("#id_tipo_periodo").val() != 0) {
      if ($("#periodo_id").val() != 0) {
        $(".spinner-modal").removeClass("hidden");
        setTimeout(function () {
          var table = $('#result_list_StockInsumos').tableToJSON();
          var periodo = $("#periodo_id").val();
          $.ajax({
            'method': 'POST',
            'url': '/ajax/save_import_stock_insumos/',
            'dataType': 'json',
            'data': {
              'period': periodo,
              'table': JSON.stringify(table)
            },
            'success': function (res) {
              if (res.response) {
                $(".spinner-modal").addClass("hidden");
                Swal.fire({
                  title: "IMPORTACIÓN DE DATOS",
                  type: "success",
                  html: "<p class='text-center'>La importación ha sido realizada exitosamente</p>"
                }).then((result) => {
                  if (result.value) {
                    window.location = '/app_imports/stock_insumos_list'
                  }
                })
              }
              if (res.error) {
                $(".spinner-modal").addClass("hidden");
                Swal.fire({
                  title: "IMPORTACIÓN DE DATOS",
                  type: "error",
                  html: "<p class='text-center'>" + res.error + "</p>"
                });
              }
            }
          });
        }, 2000);
      } else {
        Swal.fire({
          title: "IMPORTACIÓN DE DATOS",
          type: "error",
          html: "<p class='text-center'>Faltan datos por seleccionar</p>"
        });
        $(".spinner-modal").addClass("hidden");
      }
    } else {
      Swal.fire({
        title: "IMPORTACIÓN DE DATOS",
        type: "error",
        html: "<p class='text-center'>Faltan datos por seleccionar</p>"
      });
      $(".spinner-modal").addClass("hidden");
    }
  })

  $("#confirm_import_consumo_ead").on("click", function () {
    if ($("#id_tipo_periodo").val() != 0) {
      if ($("#periodo_id").val() != 0) {
        $(".spinner-modal").removeClass("hidden");
        setTimeout(function () {
          var table = $('#result_list_ConsumoEAD').tableToJSON();
          var periodo = $("#periodo_id").val();
          $.ajax({
            'method': 'POST',
            'url': '/ajax/save_import_consumo_ead/',
            'dataType': 'json',
            'data': {
              'period': periodo,
              'table': JSON.stringify(table)
            },
            'success': function (res) {
              if (res.response) {
                $(".spinner-modal").addClass("hidden");
                Swal.fire({
                  title: "IMPORTACIÓN DE DATOS",
                  type: "success",
                  html: "<p class='text-center'>La importación ha sido realizada exitosamente</p>"
                }).then((result) => {
                  if (result.value) {
                    window.location = '/app_imports/consumo_list'
                  }
                })
              }
              if (res.error) {
                $(".spinner-modal").addClass("hidden");
                Swal.fire({
                  title: "IMPORTACIÓN DE DATOS",
                  type: "error",
                  html: "<p class='text-center'>" + res.error + "</p>"
                });
              }
            }
          });
        }, 2000);
      } else {
        Swal.fire({
          title: "IMPORTACIÓN DE DATOS",
          type: "error",
          html: "<p class='text-center'>Faltan datos por seleccionar</p>"
        });
        $(".spinner-modal").addClass("hidden");
      }
    } else {
      Swal.fire({
        title: "IMPORTACIÓN DE DATOS",
        type: "error",
        html: "<p class='text-center'>Faltan datos por seleccionar</p>"
      });
      $(".spinner-modal").addClass("hidden");
    }
  })

  $("[id^='indicator-']").each(function () {
    var indicator = $(this).attr('id');
    var id = indicator.substring(10, indicator.length);
    $.ajax({
      'method': 'POST',
      'url': '/ajax/get_charts/',
      'data': {
        'indicator_id': id
      },
      'success': function (res) {
        for (let index = 0; index < res.response.length; index++) {
          const chart_id = res.response[index].chart_id;
          const size = res.response[index].size;
          $.ajax({
            'method': 'POST',
            'url': '/ajax/create_chart',
            'data': {
              'chart_id': chart_id
            },
            'success': function (res) {
              var $charts = $("#indicator-" + id);
              var legend_history = "";
              var checked = "";
              if (res.chart_dashboard) {
                checked = "checked";
              } else {
                checked = "";
              }
              if (res.history > 1) {
                legend_history = "(con historial)"
              }
              if (size == '1x1') {
                $charts.append("<div class='col-md-12'><h3 class='panel-title'>Gráfico Nro " + res.chart_id + " " + legend_history + "<div class='panel-control'><span class='text-muted'>Ver en tablero </span><input id='demo-panel-w-switch-" + res.chart_id + "' data-id='" + res.chart_id + "' class='toggle-switch' type='checkbox'" + checked + "><label for='demo-panel-w-switch-" + res.chart_id + "'></label></div></h3><hr><div id='container-" + res.chart_id + "' style='height: 300px; margin: 0 auto'></div></div>");
              }
              if (size == '1x2') {
                $charts.append("<div class='col-md-6'><h3 class='panel-title'>Gráfico Nro " + res.chart_id + " " + legend_history + "<div class='panel-control'><span class='text-muted'>Ver en tablero </span><input id='demo-panel-w-switch-" + res.chart_id + "' data-id='" + res.chart_id + "' class='toggle-switch' type='checkbox'" + checked + "><label for='demo-panel-w-switch-" + res.chart_id + "'></label></div></h3><hr><div id='container-" + res.chart_id + "' style='height: 300px; margin: 0 auto'></div></div>");
              }
              if (size == '1x3') {
                $charts.append("<div class='col-md-3'><h3 class='panel-title'>Gráfico Nro " + res.chart_id + " " + legend_history + "<div class='panel-control'><span class='text-muted'>Ver en tablero </span><input id='demo-panel-w-switch-" + res.chart_id + "' data-id='" + res.chart_id + "' class='toggle-switch' type='checkbox'" + checked + "><label for='demo-panel-w-switch-" + res.chart_id + "'></label></div></h3><hr><div id='container-" + res.chart_id + "' style='height: 300px; margin: 0 auto'></div></div>");
              }
              if (res.type_chart == "gauge") {
                var chart = new Highcharts.chart('container-' + res.chart_id, {
                  chart: {
                    type: 'gauge',
                    plotBackgroundColor: null,
                    plotBackgroundImage: null,
                    plotBorderWidth: 0,
                    plotShadow: false
                  },
                  title: {
                    text: res.title
                  },
                  subtitle: {
                    text: res.subtitle
                  },
                  credits: {
                    enabled: false
                  },
                  pane: {
                    startAngle: -90,
                    endAngle: 90,
                    background: null
                  },
                  plotOptions: {
                    gauge: {
                      dataLabels: {
                        enabled: false
                      },
                      dial: {
                        baseLength: '0%',
                        baseWidth: 10,
                        radius: '100%',
                        rearLength: '0%',
                        topWidth: 1
                      }
                    },
                    series: {
                      borderWidth: 0,
                      dataLabels: {
                        enabled: res.show_suffix,
                        formatter: function () {
                          if (res.suffix == '%') {
                            decimals = 2
                          } else {
                            decimals = 0
                          }
                          return Highcharts.numberFormat(this.y, decimals) + ' ' + res.suffix;
                        }
                      }
                    }
                  },
                  yAxis: {
                    labels: {
                      enabled: true,
                      x: 35, y: -10
                    },
                    tickPositions: [70, 90],
                    minorTickLength: 0,
                    min: 0,
                    max: 100,
                    plotBands: [{
                      from: 0,
                      to: 70,
                      color: 'rgb(192, 0, 0)', // red
                      thickness: '50%'
                    }, {
                      from: 70,
                      to: 90,
                      color: 'rgb(255, 192, 0)', // yellow
                      thickness: '50%'
                    }, {
                      from: 90,
                      to: 100,
                      color: 'rgb(155, 187, 89)', // green
                      thickness: '50%'
                    }]
                  }
                });
              } else {
                var chart = new Highcharts.chart('container-' + res.chart_id, {
                  chart: {
                    type: res.type_chart
                  },
                  colors: ["#005B94", "#0bb8cc", "#F05514", "#633ce0", "#0ec254", "#e0b500", '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce',
                    '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
                  title: {
                    text: res.title
                  },
                  subtitle: {
                    text: res.subtitle
                  },
                  credits: {
                    enabled: false
                  },
                  plotOptions: {
                    series: {
                      borderWidth: 0,
                      dataLabels: {
                        enabled: res.show_suffix,
                        formatter: function () {
                          if (res.suffix == '%') {
                            decimals = 2
                          } else {
                            decimals = 0
                          }
                          return Highcharts.numberFormat(this.y, decimals) + ' ' + res.suffix;
                        }
                      }
                    },
                    pie: {
                      allowPointSelect: true,
                      cursor: 'pointer',
                      dataLabels: {
                        enabled: true
                      },
                      showInLegend: true
                    }
                  },
                  xAxis: {
                    categories: res.categories,
                    title: {
                      text: res.xaxis_title
                    },
                    crosshair: true
                  },
                  yAxis: [{
                    title: {
                      text: ""
                    }
                  }, {
                    title: {
                      text: ""
                    }
                  }, {
                    title: {
                      text: ""
                    },
                    opposite: true,
                    labels: {
                      formatter: function () {
                        return this.value + ' %'
                      }
                    }
                  }]
                })
              }
              for (let index = 0; index < res.series.length; index++) {
                const element = res.series[index];
                chart.addSeries(element);
              }
            }
          });
        }
      }
    });
  });

  $('.grid-stack').gridstack({
    resizable: {
      handles: "false"
    }
  });

  $(".panel-body").on('click', '.toggle-switch', function () {
    var chart_id = $(this).data('id');
    var checked = false
    if ($(this).is(":checked")) {
      checked = true
    }
    $.ajax({
      'method': 'POST',
      'url': '/ajax/chart_dashboard',
      'data': {
        'chart_id': chart_id,
        'checked': checked
      },
      'success': function (res) { }
    });
  })

  $("[id^='tablero-']").each(function () {
    var id = $(this).data("id");
    var sector_id = $(this).data("sector");
    $.ajax({
      'method': 'POST',
      'url': '/ajax/get_chart/',
      'dataType': 'json',
      'data': {
        'chart_id': id
      },
      'success': function (res) {
        for (let index = 0; index < res.response.length; index++) {
          const chart_id = res.response[index].chart_id;
          const size = res.response[index].size;
          $.ajax({
            'method': 'POST',
            'url': '/ajax/create_chart_tablero',
            'data': {
              'chart_id': chart_id,
              'sector_id': sector_id
            },
            'success': function (res) {
              var $charts = $("#tablero-" + id + "-" + sector_id);
              $charts.html("");
              $charts.append("<div data-period='" + res.period_id + "' id='container-" + res.chart_id + "-" + sector_id + "' style='height: 280px; margin: 0 auto'></div>");
              if (res.type_chart == "gauge") {
                $("#container-" + res.chart_id + "-" + sector_id).css("height", "400px");
                var chart = new Highcharts.chart('container-' + res.chart_id + '-' + sector_id, {
                  chart: {
                    type: 'gauge',
                    plotBackgroundColor: null,
                    plotBackgroundImage: null,
                    plotBorderWidth: 0,
                    plotShadow: false
                  },
                  title: {
                    text: res.title
                  },
                  subtitle: {
                    text: res.subtitle
                  },
                  credits: {
                    enabled: false
                  },
                  pane: {
                    startAngle: -90,
                    endAngle: 90,
                    background: null
                  },
                  plotOptions: {
                    gauge: {
                      dataLabels: {
                        enabled: false
                      },
                      dial: {
                        baseLength: '0%',
                        baseWidth: 10,
                        radius: '100%',
                        rearLength: '0%',
                        topWidth: 1
                      }
                    },
                    series: {
                      borderWidth: 0,
                      dataLabels: {
                        enabled: res.show_suffix,
                        formatter: function () {
                          if (res.suffix == '%') {
                            decimals = 2
                          } else {
                            decimals = 0
                          }
                          return Highcharts.numberFormat(this.y, decimals) + ' ' + res.suffix;
                        }
                      }
                    }
                  },
                  yAxis: {
                    labels: {
                      enabled: true,
                      distance: '120%',
                      format: '{value}%'
                    },
                    tickPositions: [70, 90],
                    minorTickLength: 0,
                    min: 0,
                    max: 100,
                    plotBands: [{
                      from: 0,
                      to: 70,
                      color: 'rgb(192, 0, 0)', // red
                      thickness: '50%'
                    }, {
                      from: 70,
                      to: 90,
                      color: 'rgb(255, 192, 0)', // yellow
                      thickness: '50%'
                    }, {
                      from: 90,
                      to: 100,
                      color: 'rgb(155, 187, 89)', // green
                      thickness: '50%'
                    }]
                  }
                });
              } else {
                var chart = new Highcharts.chart('container-' + res.chart_id + '-' + sector_id, {
                  chart: {
                    type: res.type_chart
                  },
                  colors: ["#005B94", "#0bb8cc", "#F05514", "#633ce0", "#0ec254", "#e0b500", '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce',
                    '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
                  title: {
                    text: res.title
                  },
                  subtitle: {
                    text: res.subtitle
                  },
                  credits: {
                    enabled: false
                  },
                  plotOptions: {
                    series: {
                      borderWidth: 0,
                      dataLabels: {
                        enabled: res.show_suffix,
                        formatter: function () {
                          if (res.suffix == '%') {
                            decimals = 2
                          } else {
                            decimals = 0
                          }
                          return Highcharts.numberFormat(this.y, decimals) + ' ' + res.suffix;
                        }
                      }
                    },
                    pie: {
                      allowPointSelect: true,
                      cursor: 'pointer',
                      dataLabels: {
                        enabled: true
                      },
                      showInLegend: true
                    }
                  },
                  xAxis: {
                    categories: res.categories,
                    title: {
                      text: res.xaxis_title
                    },
                    crosshair: true
                  },
                  yAxis: [{
                    title: {
                      text: ""
                    }
                  }, {
                    title: {
                      text: ""
                    }
                  }, {
                    title: {
                      text: ""
                    },
                    opposite: true,
                    labels: {
                      formatter: function () {
                        return this.value + ' %'
                      }
                    }
                  }]
                })
              }
              for (let index = 0; index < res.series.length; index++) {
                const element = res.series[index];
                chart.addSeries(element);
              }
            }
          });
        }
      },
      'error': function (err) {
        console.log(err);
      }
    });
  });

  $("#delete_chart").on("click", function () {
    var chart_id = $(this).data("id");
    Swal.fire({
      title: "Configuración de gráficos",
      type: "warning",
      html: "<p class='text-center'>¿desea eliminar el gráfico seleccionado?</p>",
      showCancelButton: true,
      confirmButtonText: 'Eliminar'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          'method': 'POST',
          'url': '/ajax/delete_chart/',
          'data': {
            'chart_id': chart_id
          },
          'success': function (res) {
            if (res.response) {
              Swal.fire({
                title: "Configuración de gráficos",
                type: "success",
                html: "<p class='text-center'>¡El gráfico ha sido eliminado exitosamente!</p>"
              }).then((result) => {
                window.location = '/app_kpi/chart_list'
              });
            }
          }
        });
      }
    })
  })

  $("#delete_sector").on("click", function () {
    var sector_id = $(this).data("id");
    Swal.fire({
      title: "Maestro de sectores",
      type: "warning",
      html: "<p class='text-center'>¿desea eliminar el sector seleccionado?</p>",
      showCancelButton: true,
      confirmButtonText: 'Eliminar'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          'method': 'POST',
          'url': '/ajax/delete_sector/',
          'data': {
            'sector_id': sector_id
          },
          'success': function (res) {
            if (res.response) {
              Swal.fire({
                title: "Maestro de sectores",
                type: "success",
                html: "<p class='text-center'>¡El sector ha sido eliminado exitosamente!</p>"
              }).then((result) => {
                window.location = '/app_master/sector_list'
              });
            }
          }
        });
      }
    })
  })

  $("#generatePdf").on("click", function () {
    var element = document.getElementById("content-container");
    html2canvas(element).then(function (canvas) {
      var contentWidth = canvas.width;
      var contentHeight = canvas.height;
      var position = 20,
        x = 20,
        y = 0;
      var pageHeight = (contentWidth / 595.28 * 841.89);
      var restHeight = contentHeight;
      var imgWidth = 841.89 - position * 2;
      var imgHeight = imgWidth / contentWidth * contentHeight - position * 2;
      var pageData = canvas.toDataURL('image/jpeg', 1.0);
      var doc = new jsPDF('landscape', 'pt', 'a4');

      if (restHeight < pageHeight) { doc.addImage(pageData, 'JPEG', x, position, imgWidth, imgHeight); } else {
        while (restHeight > 0) {
          doc.addImage(pageData, 'JPEG', x, position + y, imgWidth, imgHeight);
          restHeight -= pageHeight;
          y -= 841.89;
          if (restHeight > 0) {
            doc.addPage();
          }
        }
      }
      doc.save('dashboard.pdf');
    })
  });


  $("#nameDashboard").on("keydown", function () {
    var name = $(this).val();
    $.ajax({
      'method': 'POST',
      'url': '/ajax/check_name_dashboard/',
      'data': {
        'name': name
      },
      'success': function (res) { }
    });
  })

  $("#a_before_period").on("click", function () {
    var num_view = $("#num_view").val();
    num_view = parseInt(num_view) - 1;
    $("#num_view").val(num_view);
    if (num_view < 0) {
      $("#after_period").removeClass("disabled");
      $("#after_period").children("a_after_period").removeClass("disabled");
    } else {
      $("#after_period").addClass("disabled");
      $("#after_period").children("a_after_period").addClass("disabled");
    }
    $("[id^='tablero-']").each(function () {
      var id = $(this).data("id");
      var sector_id = $(this).data("sector");
      var period_id = $(this).children("div").data("period");
      $(this).html("<div class='load7'><div class='loader'></div></div>");
      $.ajax({
        'method': 'POST',
        'url': '/ajax/get_chart/',
        'data': {
          'chart_id': id
        },
        'success': function (res) {
          for (let index = 0; index < res.response.length; index++) {
            const chart_id = res.response[index].chart_id;
            const size = res.response[index].size;
            $.ajax({
              'method': 'POST',
              'url': '/ajax/create_chart_tablero/before',
              'data': {
                'chart_id': chart_id,
                'sector_id': sector_id,
                'period_id': period_id
              },
              'success': function (res) {
                var $charts = $("#tablero-" + id + "-" + sector_id);
                $charts.html("");
                $charts.append("<div data-period='" + res.period_id + "' id='container-" + res.chart_id + "-" + sector_id + "' style='height: 280px; margin: 0 auto'></div>");
                if (res.type_chart == "gauge") {
                  $("#container-" + res.chart_id + "-" + sector_id).css("height", "400px");
                  var chart = new Highcharts.chart('container-' + res.chart_id + '-' + sector_id, {
                    chart: {
                      type: 'gauge',
                      plotBackgroundColor: null,
                      plotBackgroundImage: null,
                      plotBorderWidth: 0,
                      plotShadow: false
                    },
                    title: {
                      text: res.title
                    },
                    subtitle: {
                      text: res.subtitle
                    },
                    credits: {
                      enabled: false
                    },
                    pane: {
                      startAngle: -90,
                      endAngle: 90,
                      background: null
                    },
                    plotOptions: {
                      gauge: {
                        dataLabels: {
                          enabled: false
                        },
                        dial: {
                          baseLength: '0%',
                          baseWidth: 10,
                          radius: '100%',
                          rearLength: '0%',
                          topWidth: 1
                        }
                      },
                      series: {
                        borderWidth: 0,
                        dataLabels: {
                          enabled: res.show_suffix,
                          formatter: function () {
                            if (res.suffix == '%') {
                              decimals = 2
                            } else {
                              decimals = 0
                            }
                            return Highcharts.numberFormat(this.y, decimals) + ' ' + res.suffix;
                          }
                        }
                      }
                    },
                    yAxis: {
                      labels: {
                        enabled: true,
                        distance: '120%',
                        format: '{value}%'
                      },
                      tickPositions: [70, 90],
                      minorTickLength: 0,
                      min: 0,
                      max: 100,
                      plotBands: [{
                        from: 0,
                        to: 70,
                        color: 'rgb(192, 0, 0)', // red
                        thickness: '50%'
                      }, {
                        from: 70,
                        to: 90,
                        color: 'rgb(255, 192, 0)', // yellow
                        thickness: '50%'
                      }, {
                        from: 90,
                        to: 100,
                        color: 'rgb(155, 187, 89)', // green
                        thickness: '50%'
                      }]
                    }
                  });
                } else {
                  $charts.append("<div data-period='" + res.period_id + "' id='container-" + res.chart_id + "-" + sector_id + "' style='height: 280px; margin: 0 auto'></div>");
                  var chart = new Highcharts.chart('container-' + res.chart_id + '-' + sector_id, {
                    chart: {
                      type: res.type_chart
                    },
                    colors: ["#005B94", "#F05514", "#0bb8cc", "#633ce0", "#0ec254", "#e0b500", '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce',
                      '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
                    title: {
                      text: res.title
                    },
                    subtitle: {
                      text: res.subtitle
                    },
                    credits: {
                      enabled: false
                    },
                    plotOptions: {
                      series: {
                        borderWidth: 0,
                        dataLabels: {
                          enabled: res.show_suffix,
                          formatter: function () {
                            if (res.suffix == '%') {
                              decimals = 2
                            } else {
                              decimals = 0
                            }
                            return Highcharts.numberFormat(this.y, decimals) + ' ' + res.suffix;
                          }
                        }
                      },
                      pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                          enabled: true
                        },
                        showInLegend: true
                      }
                    },
                    xAxis: {
                      categories: res.categories,
                      title: {
                        text: res.xaxis_title
                      }
                    },
                    yAxis: {
                      title: {
                        text: res.yaxis_title
                      }
                    }
                  })
                }
                for (let index = 0; index < res.series.length; index++) {
                  const element = res.series[index];
                  chart.addSeries(element);
                }
              }
            });
          }

        }
      });
    });
  })

  $("#a_after_period").on("click", function () {
    var num_view = $("#num_view").val();
    num_view = parseInt(num_view) + 1;
    $("#num_view").val(num_view);
    if (num_view < 0) {
      $("#after_period").removeClass("disabled");
      $("#after_period").children("a_after_period").removeClass("disabled");
    } else {
      $("#after_period").addClass("disabled");
      $("#after_period").children("a_after_period").addClass("disabled");
    }
    $("[id^='tablero-']").each(function () {
      var id = $(this).data("id");
      var sector_id = $(this).data("sector");
      var period_id = $(this).children("div").data("period");
      $(this).html("<div class='load7'><div class='loader'></div></div>");
      $.ajax({
        'method': 'POST',
        'url': '/ajax/get_chart/',
        'data': {
          'chart_id': id,
          'sector_id': sector_id,
          'period_id': period_id
        },
        'success': function (res) {
          for (let index = 0; index < res.response.length; index++) {
            const chart_id = res.response[index].chart_id;
            const size = res.response[index].size;
            $.ajax({
              'method': 'POST',
              'url': '/ajax/create_chart_tablero/after',
              'data': {
                'chart_id': chart_id,
                'sector_id': sector_id,
                'period_id': period_id
              },
              'success': function (res) {
                var $charts = $("#tablero-" + id + "-" + sector_id);
                $charts.html("");
                $charts.append("<div data-period='" + res.period_id + "' id='container-" + res.chart_id + "-" + sector_id + "' style='height: 280px; margin: 0 auto'></div>");
                if (res.type_chart == "gauge") {
                  $("#container-" + res.chart_id + "-" + sector_id).css("height", "400px");
                  var chart = new Highcharts.chart('container-' + res.chart_id + '-' + sector_id, {
                    chart: {
                      type: 'gauge',
                      plotBackgroundColor: null,
                      plotBackgroundImage: null,
                      plotBorderWidth: 0,
                      plotShadow: false
                    },
                    title: {
                      text: res.title
                    },
                    subtitle: {
                      text: res.subtitle
                    },
                    credits: {
                      enabled: false
                    },
                    pane: {
                      startAngle: -90,
                      endAngle: 90,
                      background: null
                    },
                    plotOptions: {
                      gauge: {
                        dataLabels: {
                          enabled: false
                        },
                        dial: {
                          baseLength: '0%',
                          baseWidth: 10,
                          radius: '100%',
                          rearLength: '0%',
                          topWidth: 1
                        }
                      },
                      series: {
                        borderWidth: 0,
                        dataLabels: {
                          enabled: res.show_suffix,
                          formatter: function () {
                            if (res.suffix == '%') {
                              decimals = 2
                            } else {
                              decimals = 0
                            }
                            return Highcharts.numberFormat(this.y, decimals) + ' ' + res.suffix;
                          }
                        }
                      }
                    },
                    yAxis: {
                      labels: {
                        enabled: true,
                        distance: '120%',
                        format: '{value}%'
                      },
                      tickPositions: [70, 90],
                      minorTickLength: 0,
                      min: 0,
                      max: 100,
                      plotBands: [{
                        from: 0,
                        to: 70,
                        color: 'rgb(192, 0, 0)', // red
                        thickness: '50%'
                      }, {
                        from: 70,
                        to: 90,
                        color: 'rgb(255, 192, 0)', // yellow
                        thickness: '50%'
                      }, {
                        from: 90,
                        to: 100,
                        color: 'rgb(155, 187, 89)', // green
                        thickness: '50%'
                      }]
                    }
                  });
                } else {
                  $charts.append("<div data-period='" + res.period_id + "' id='container-" + res.chart_id + "-" + sector_id + "' style='height: 280px; margin: 0 auto'></div>");
                  var chart = new Highcharts.chart('container-' + res.chart_id + '-' + sector_id, {
                    chart: {
                      type: res.type_chart
                    },
                    colors: ["#005B94", "#F05514", "#0bb8cc", "#633ce0", "#0ec254", "#e0b500", '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce',
                      '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
                    title: {
                      text: res.title
                    },
                    subtitle: {
                      text: res.subtitle
                    },
                    credits: {
                      enabled: false
                    },
                    plotOptions: {
                      series: {
                        borderWidth: 0,
                        dataLabels: {
                          enabled: res.show_suffix,
                          formatter: function () {
                            if (res.suffix == '%') {
                              decimals = 2
                            } else {
                              decimals = 0
                            }
                            return Highcharts.numberFormat(this.y, decimals) + ' ' + res.suffix;
                          }
                        }
                      },
                      pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                          enabled: true
                        },
                        showInLegend: true
                      }
                    },
                    xAxis: {
                      categories: res.categories,
                      title: {
                        text: res.xaxis_title
                      }
                    },
                    yAxis: {
                      title: {
                        text: res.yaxis_title
                      }
                    }
                  })
                }
                for (let index = 0; index < res.series.length; index++) {
                  const element = res.series[index];
                  chart.addSeries(element);
                }
              }
            });
          }

        }
      });
    });
  })

  $(".remove-graph").on("click", function () {
    var chart_id = $(this).data("id");
    $("#grid-stack-chart-" + chart_id).remove();
  });

  $("#confirm_import_reunion_volumen").on("click", function () {
    if ($("#id_tipo_periodo").val() != 0 && $("#id_sector").val() != 0) {
      if ($("#periodo_id").val() != 0) {
        $(".spinner-modal").removeClass("hidden");
        setTimeout(function () {
          var table = $('#result_list_ReunionVolumen').tableToJSON();
          var periodo = $("#periodo_id").val();
          var sector = $("#sector_id").val();
          $.ajax({
            'method': 'POST',
            'url': '/ajax/save_import_reunion_volumen/',
            'dataType': 'json',
            'data': {
              'period': periodo,
              'sector': sector,
              'table': JSON.stringify(table)
            },
            'success': function (res) {
              if (res.response) {
                $(".spinner-modal").addClass("hidden");
                Swal.fire({
                  title: "IMPORTACIÓN DE DATOS",
                  type: "success",
                  html: "<p class='text-center'>La importación ha sido realizada exitosamente</p>"
                }).then((result) => {
                  if (result.value) {
                    window.location = '/app_imports/reunion_volumen_list'
                  }
                })
              }
              if (res.error) {
                $(".spinner-modal").addClass("hidden");
                Swal.fire({
                  title: "IMPORTACIÓN DE DATOS",
                  type: "error",
                  html: "<p class='text-center'>" + res.error + "</p>"
                });
              }
            }
          });
        }, 2000);
      } else {
        Swal.fire({
          title: "IMPORTACIÓN DE DATOS",
          type: "error",
          html: "<p class='text-center'>Faltan datos por seleccionar</p>"
        });
        $(".spinner-modal").addClass("hidden");
      }
    } else {
      Swal.fire({
        title: "IMPORTACIÓN DE DATOS",
        type: "error",
        html: "<p class='text-center'>Faltan datos por seleccionar</p>"
      });
      $(".spinner-modal").addClass("hidden");
    }
  })

});