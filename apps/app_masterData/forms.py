from django import forms
from .models import *


class PeriodTypeForm(forms.ModelForm):

    class Meta:
        model = PeriodType
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(PeriodTypeForm, self).__init__(*args, **kwargs)
        self.fields['period'].widget.attrs['class'] = 'form-control'


class PeriodForm(forms.ModelForm):

    class Meta:
        model = Period
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(PeriodForm, self).__init__(*args, **kwargs)
        self.fields['period_type'].widget.attrs['class'] = 'form-control'
        self.fields['name'].widget.attrs['class'] = 'form-control'


class SectorForm(forms.ModelForm):

    class Meta:
        model = Sector
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(SectorForm, self).__init__(*args, **kwargs)
        self.fields['code'].widget.attrs['class'] = 'form-control'
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['id'].widget.attrs['class'] = 'form-control'
