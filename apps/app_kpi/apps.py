from django.apps import AppConfig


class AppKpiConfig(AppConfig):
    name = 'app_kpi'
