$(document).on('nifty.ready', function () {

    $.fn.DataTable.ext.pager.numbers_length = 5;

    $('.datatable-list').dataTable({
        "responsive": true,
        "language": {
            "url": '//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json',
            "paginate": {
                "previous": '<i class="demo-psi-arrow-left"></i>',
                "next": '<i class="demo-psi-arrow-right"></i>'
            }
        },
        "columnDefs": [
            { "width": "20px", "targets": 0 }
        ]
    });

});
