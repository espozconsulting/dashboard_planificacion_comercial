# Generated by Django 2.2 on 2019-07-01 14:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app_masterData', '0004_segmentacioninsumo'),
        ('app_imports', '0005_remove_reunionvolumen_stock'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='consumoead',
            name='center',
        ),
        migrations.AddField(
            model_name='consumoead',
            name='plant',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='app_masterData.Plants'),
            preserve_default=False,
        ),
    ]
