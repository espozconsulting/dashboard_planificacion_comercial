from django.views.generic import ListView
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponse
from django.db.models import Avg

from django.contrib.auth.models import User

from .models import *
from .forms import *
from apps.app_kpi.models import *
from apps.app_masterData.models import *

import inspect
import json


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "base.html"

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context["indicators"] = Indicator.objects.all()
        return context


class ImportFileListView(LoginRequiredMixin, ListView):
    model = ImportFile
    template_name = "app_settings/list_import_file.html"


class ImportFileCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    model = ImportFile
    form_class = ImportFileForm
    template_name = "app_settings/edit_import_file.html"
    success_url = "/app_settings/import_file_list"
    success_message = "%(name)s ha sido creado satisfactoriamente"


class ImportFileEditView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = ImportFile
    form_class = ImportFileForm
    template_name = "app_settings/edit_import_file.html"
    success_url = "/app_settings/import_file_list"
    success_message = "%(name)s ha sido actualizado satisfactoriamente"


class HomologationListView(LoginRequiredMixin, ListView):
    model = Homologation
    template_name = "app_settings/list_homologation.html"


class HomologationCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    model = Homologation
    fields = '__all__'
    template_name = "app_settings/create_homologation.html"
    success_url = "/app_settings/homologation_list"
    success_message = "%(name)s ha sido creado satisfactoriamente"

    def get_context_data(self, **kwargs):
        context = super(HomologationCreateView,
                        self).get_context_data(**kwargs)
        context["archivos"] = ImportFile.objects.all()
        return context


class HomologationEditView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Homologation
    form_class = HomologationForm
    template_name = "app_settings/edit_homologation.html"
    success_url = "/app_settings/homologation_list"
    success_message = "%(import_file)s ha sido actualizado satisfactoriamente"


class DashboardView(TemplateView):
    template_name = "dashboard.html"

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context["sectores"] = []
        ids = []
        sectores = Sector.objects.all().order_by('id')
        for x in sectores:
            graficos = DashboardCharts.objects.values('chart_id', 'chart__size__name', 'gs_x', 'gs_y').filter(
                dashboard_id=5, show=True)
            ids = []
            for i in graficos:
                ids.append({
                    'id_chart': i['chart_id'],
                    'size': i['chart__size__name'],
                    'gs_x': i['gs_x'],
                    'gs_y': i['gs_y']
                })
            context["sectores"].append({
                'code': x.code,
                'name': x.name,
                'charts': ids
            })
        context["num_view"] = 0
        return context
