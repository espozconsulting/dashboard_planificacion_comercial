from django.contrib import admin
from .models import *


@admin.register(Indicator)
class IndicatorAdmin(admin.ModelAdmin):
    pass


@admin.register(Chart)
class ChartAdmin(admin.ModelAdmin):
    pass
