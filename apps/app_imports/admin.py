from django.contrib import admin
from .models import *


@admin.register(BDProduccion)
class BDProduccionAdmin(admin.ModelAdmin):
    pass


@admin.register(StockInsumos)
class StockInsumosAdmin(admin.ModelAdmin):
    pass
