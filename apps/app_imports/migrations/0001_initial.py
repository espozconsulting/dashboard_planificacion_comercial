# Generated by Django 2.2 on 2019-06-03 05:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('app_masterData', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='StockMeta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sector', models.CharField(default='', max_length=100, verbose_name='Sector')),
                ('code', models.CharField(max_length=50, verbose_name='Cod. SAP')),
                ('description', models.CharField(max_length=250, verbose_name='Descripción')),
                ('stock', models.FloatField(default=0, verbose_name='Stock Meta')),
                ('period', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_masterData.Period', verbose_name='Periodo')),
            ],
        ),
        migrations.CreateModel(
            name='StockInsumos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('center', models.CharField(max_length=150, verbose_name='Centro')),
                ('code', models.CharField(max_length=50, verbose_name='Cod. SAP')),
                ('description', models.CharField(max_length=250, verbose_name='Descripción')),
                ('stock', models.FloatField(default=0, verbose_name='Stock')),
                ('umb', models.CharField(max_length=50, verbose_name='UMB')),
                ('free', models.FloatField(default=0, verbose_name='Valor libre util.')),
                ('almacen', models.CharField(max_length=150, verbose_name='Almacén')),
                ('group', models.CharField(max_length=150, verbose_name='Gpo.artíc.')),
                ('period', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_masterData.Period', verbose_name='Periodo')),
            ],
        ),
        migrations.CreateModel(
            name='StockHomologado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('almacen', models.CharField(max_length=150, verbose_name='Almacén')),
                ('level2', models.CharField(max_length=150, verbose_name='Nivel 2')),
                ('code', models.CharField(max_length=50, verbose_name='Cod. SAP')),
                ('description', models.CharField(max_length=250, verbose_name='Descripción')),
                ('date_slaughter', models.DateField(null=True, verbose_name='Fecha Faenación')),
                ('tagged_date', models.DateField(null=True, verbose_name='Fecha Etiquetado')),
                ('supplying_center', models.CharField(max_length=150, verbose_name='Centro Suministrador')),
                ('center', models.CharField(max_length=150, verbose_name='Centro')),
                ('destiny', models.CharField(max_length=50, verbose_name='Destino')),
                ('free_stock', models.FloatField(default=0, verbose_name='Stock Libre')),
                ('blocked_stock', models.FloatField(default=0, verbose_name='Stock Bloqueado')),
                ('period', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_masterData.Period', verbose_name='Periodo')),
                ('sector', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_masterData.Sector', verbose_name='Sector')),
            ],
        ),
        migrations.CreateModel(
            name='ProgramaProduccion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('center', models.CharField(max_length=50, verbose_name='Centro Productivo')),
                ('date_proccess', models.DateField(verbose_name='Fecha de proceso')),
                ('level2', models.CharField(max_length=150, verbose_name='Nivel 2')),
                ('kg_original', models.FloatField(default=0, verbose_name='KG Plan Original')),
                ('kg_modified', models.FloatField(default=0, verbose_name='KG Plan Modificado')),
                ('kg_real', models.FloatField(default=0, verbose_name='KG Real')),
                ('period', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_masterData.Period', verbose_name='Periodo')),
                ('sector', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_masterData.Sector', verbose_name='Sector')),
            ],
        ),
        migrations.CreateModel(
            name='ConsumoEAD',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hour', models.CharField(max_length=150, verbose_name='Hora de entrada')),
                ('classmov', models.CharField(max_length=50, verbose_name='Clase de movimiento')),
                ('document', models.CharField(max_length=50, verbose_name='Documento material')),
                ('center', models.CharField(max_length=50, verbose_name='Centro')),
                ('name', models.CharField(max_length=50, verbose_name='Nombre 1')),
                ('almacen', models.CharField(max_length=50, verbose_name='Almacén')),
                ('lote', models.CharField(max_length=50, verbose_name='Lote')),
                ('code', models.CharField(max_length=50, verbose_name='Cod. SAP')),
                ('description', models.CharField(max_length=250, verbose_name='Descripción')),
                ('date', models.DateField(verbose_name='Fecha')),
                ('quantity_um', models.FloatField(default=0, verbose_name='Ctd.en UM entrada')),
                ('um', models.CharField(max_length=50, verbose_name='UM')),
                ('quantity', models.FloatField(default=0, verbose_name='Cantidad')),
                ('um_base', models.CharField(max_length=50, verbose_name='UM Base')),
                ('period', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_masterData.Period', verbose_name='Periodo')),
            ],
        ),
        migrations.CreateModel(
            name='BDProduccion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=50, verbose_name='Cod. SAP')),
                ('description', models.CharField(max_length=250, verbose_name='Descripción')),
                ('date', models.DateField(verbose_name='Fecha')),
                ('k_producer', models.FloatField(default=0, verbose_name='Kg a producir')),
                ('k_real', models.FloatField(default=0, verbose_name='Kg reales')),
                ('plant', models.CharField(max_length=150, verbose_name='Planta')),
                ('productive_cap', models.FloatField(default=0, verbose_name='Tope Productivo')),
                ('period', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_masterData.Period', verbose_name='Periodo')),
                ('sector', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_masterData.Sector')),
            ],
        ),
    ]
