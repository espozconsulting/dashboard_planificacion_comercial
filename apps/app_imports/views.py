from django.views.generic import ListView
from django.views.generic.base import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import connection
from django.shortcuts import render
from django.apps import apps
from django.core import serializers
from django.http import HttpResponse
from openpyxl import load_workbook
from io import BytesIO
from apps.app_imports.models import *
from apps.app_settings.models import *
from apps.app_kpi.models import *

import json
import datetime


class ProductionProgramListView(LoginRequiredMixin, ListView):
    model = ProgramaProduccion
    template_name = "app_imports/programa_produccion_list.html"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(ProductionProgramListView,
                        self).get_context_data(**kwargs)
        context["filter"] = self.request.GET.get('filter', None)
        return context

    def get_queryset(self):
        filter_val = self.request.GET.get('filter', None)
        if filter_val:
            context = ProgramaProduccion.objects.filter(code=filter_val)
        else:
            context = ProgramaProduccion.objects.all()
        return context


class ProductionProgramImport(LoginRequiredMixin, TemplateView):
    template_name = "app_imports/programa_produccion_import.html"

    def get_context_data(self, **kwargs):
        context = super(ProductionProgramImport,
                        self).get_context_data(**kwargs)
        context["model"] = "ProgramaProduccion"
        return context


class DBProductionListView(LoginRequiredMixin, ListView):
    model = BDProduccion
    template_name = "app_imports/bd_produccion_list.html"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(DBProductionListView, self).get_context_data(**kwargs)
        context["filter"] = self.request.GET.get('filter', None)
        return context

    def get_queryset(self):
        filter_val = self.request.GET.get('filter', None)
        if filter_val:
            context = BDProduccion.objects.filter(code=filter_val)
        else:
            context = BDProduccion.objects.all()
        return context


class DBProductionImport(LoginRequiredMixin, TemplateView):
    template_name = "app_imports/bd_produccion_import.html"

    def get_context_data(self, **kwargs):
        context = super(DBProductionImport, self).get_context_data(**kwargs)
        context["model"] = "BDProduccion"
        return context


class StockHomologadoListView(LoginRequiredMixin, ListView):
    model = StockHomologado
    template_name = "app_imports/stock_homologado_list.html"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(StockHomologadoListView,
                        self).get_context_data(**kwargs)
        context["filter"] = self.request.GET.get('filter', None)
        return context

    def get_queryset(self):
        filter_val = self.request.GET.get('filter', None)
        if filter_val:
            context = StockHomologado.objects.filter(code=filter_val)
        else:
            context = StockHomologado.objects.all()
        return context


class StockHomologadoImport(LoginRequiredMixin, TemplateView):
    template_name = "app_imports/stock_homologado_import.html"

    def get_context_data(self, **kwargs):
        context = super(StockHomologadoImport, self).get_context_data(**kwargs)
        context["model"] = "StockHomologado"
        return context


class StockMetaListView(LoginRequiredMixin, ListView):
    model = StockMeta
    template_name = "app_imports/stock_meta_list.html"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(StockMetaListView, self).get_context_data(**kwargs)
        context["filter"] = self.request.GET.get('filter', None)
        return context

    def get_queryset(self):
        filter_val = self.request.GET.get('filter', None)
        if filter_val:
            context = StockMeta.objects.filter(code=filter_val)
        else:
            context = StockMeta.objects.all()
        return context


class StockMetaImport(LoginRequiredMixin, TemplateView):
    template_name = "app_imports/stock_meta_import.html"

    def get_context_data(self, **kwargs):
        context = super(StockMetaImport, self).get_context_data(**kwargs)
        context["model"] = "StockMeta"
        return context


class StockInsumosListView(LoginRequiredMixin, ListView):
    model = StockInsumos
    template_name = "app_imports/stock_insumos_list.html"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(StockInsumosListView, self).get_context_data(**kwargs)
        context["filter"] = self.request.GET.get('filter', None)
        return context

    def get_queryset(self):
        filter_val = self.request.GET.get('filter', None)
        if filter_val:
            context = StockInsumos.objects.filter(code=filter_val)
        else:
            context = StockInsumos.objects.all()
        return context


class StockInsumosImport(LoginRequiredMixin, TemplateView):
    template_name = "app_imports/stock_insumos_import.html"

    def get_context_data(self, **kwargs):
        context = super(StockInsumosImport, self).get_context_data(**kwargs)
        context["model"] = "StockInsumos"
        return context


class ConsumoEADListView(LoginRequiredMixin, ListView):
    model = ConsumoEAD
    template_name = "app_imports/consumo_ead_list.html"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(ConsumoEADListView, self).get_context_data(**kwargs)
        context["filter"] = self.request.GET.get('filter', None)
        return context

    def get_queryset(self):
        filter_val = self.request.GET.get('filter', None)
        if filter_val:
            context = ConsumoEAD.objects.filter(code=filter_val)
        else:
            context = ConsumoEAD.objects.all()
        return context


class ConsumoEADImport(LoginRequiredMixin, TemplateView):
    template_name = "app_imports/consumo_ead_import.html"

    def get_context_data(self, **kwargs):
        context = super(ConsumoEADImport, self).get_context_data(**kwargs)
        context["model"] = "ConsumoEAD"
        return context


class ReunionVolumenListView(LoginRequiredMixin, ListView):
    model = ReunionVolumen
    template_name = "app_imports/reunion_volumen_list.html"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(ReunionVolumenListView, self).get_context_data(**kwargs)
        context["filter"] = self.request.GET.get('filter', None)
        return context

    def get_queryset(self):
        filter_val = self.request.GET.get('filter', None)
        if filter_val:
            context = ReunionVolumen.objects.filter(code=filter_val)
        else:
            context = ReunionVolumen.objects.all()
        return context


class ReunionVolumenImport(LoginRequiredMixin, TemplateView):
    template_name = "app_imports/reunion_volumen_import.html"

    def get_context_data(self, **kwargs):
        context = super(ReunionVolumenImport, self).get_context_data(**kwargs)
        context["model"] = "ReunionVolumen"
        return context


def get_columns(request):
    if request.POST:
        id = request.POST['id']
        modelo = ImportFile.objects.all().filter(id=id)
        for mod in modelo:
            model = apps.get_model('app_imports', mod.name_model)
        context = []
        attributes = model._meta.get_fields()
        for field in attributes:
            if field.name != 'id':
                context.append({
                    'column': field.verbose_name
                })
    json_data = json.dumps({'response': context})
    return HttpResponse(json_data, content_type='application/json')


def get_periods(request):
    if request.POST:
        context = []
        id = request.POST['id']
        periodos = Period.objects.all().filter(period_type_id=id)
        for target_list in periodos:
            context.append({
                'id': target_list.id,
                'period': target_list.name
            })
    json_data = json.dumps({'response': context})
    return HttpResponse(json_data, content_type='application/json')


def save_homologation(request):
    if request.method == 'POST':
        element = Homologation()
        element.import_file_id = request.POST['import_file_id']
        element.entry_header = request.POST['from']
        element.db_header = request.POST['to']
        element.save()
    json_data = json.dumps({'response': 'ok'})
    return HttpResponse(json_data, content_type='application/json')


def import_data_production_program(request):
    context = []
    context_header = []
    context_tipoPeriodo = []
    context_sector = []
    dictionary = my_dictionary()
    if request.method == 'POST':
        file_in_memory = request.FILES['import_file'].read()
        wb = load_workbook(filename=BytesIO(
            file_in_memory), data_only=True)
        ws = wb.active
        model = request.POST['model']
        data = ImportFile.objects.all().filter(name_model=model)
        context_tipoPeriodo = PeriodType.objects.all()
        context_sector = Sector.objects.all()
        first_row = 1
        for x in data:
            first_row = x.first_row
            data = Homologation.objects.all().filter(
                import_file_id=x.id).order_by('entry_header')
            for target_list in range(first_row, ws.max_row + 1):
                dictionary = my_dictionary()
                for i in data:
                    if target_list == first_row:
                        context_header.append({
                            'header': i.db_header
                        })
                    dictionary.add(i.entry_header, ws.cell(
                        row=target_list, column=i.entry_header + 1).value)
                context.append(dictionary)
    return render(request, 'app_imports/programa_produccion_result.html', {'object_list': context, 'header': context_header, 'tipoPeriodo': context_tipoPeriodo, 'sector': context_sector})


def save_import_programa_produccion(request):
    if request.POST and request.is_ajax():
        count = 1
        json_obj = json.loads(request.POST.get('table'))
        for x in json_obj:
            try:
                fecha_proceso = x['Fecha de proceso'].replace(".", "-")
                fecha_proceso = datetime.datetime.strptime(fecha_proceso, '%d-%m-%Y')
                if ProgramaProduccion.objects.filter(period_id=request.POST['period'], center=x['Centro Productivo'], date_proccess=fecha_proceso, code=x['Cod. SAP']).exists():
                    delete = ProgramaProduccion.objects.filter(
                        period_id=request.POST['period'], center=x['Centro Productivo'], date_proccess=fecha_proceso, code=x['Cod. SAP']).delete()
                model = ProgramaProduccion()
                model.sector_id = int(x['Sector'])
                model.period_id = request.POST['period']
                model.code = x['Cod. SAP']
                model.description = x['Descripción']
                model.center = x['Centro Productivo']
                model.destiny = x['Pais']
                model.date_proccess = fecha_proceso
                model.level2 = x['Nivel 2']
                if x['KG Plan Original'] != "None":
                    model.kg_original = float(x['KG Plan Original'].replace(".", "").replace(",", "."))
                else:
                    model.kg_original = 0
                if x['KG Plan Modificado'] != "None":
                    model.kg_modified = float(x['KG Plan Modificado'].replace(".", "").replace(",", "."))
                else:
                    model.kg_modified = 0
                if x['KG Real'] != "None":
                    model.kg_real = float(x['KG Real'].replace(".", "").replace(",", "."))
                else:
                    model.kg_real = 0
                model.save()
                count += 1
            except Exception as e:
                json_data = json.dumps({'error': str(e) + ', line: ' + str(count)})
                return HttpResponse(json_data, content_type='application/json')
        periodo = request.POST['period']
        with connection.cursor() as cursor:
            cursor.callproc('CONTROL_PRODUCCION_202', [periodo])
            cursor.callproc('CONTROL_PRODUCCION_203', [periodo])
            cursor.callproc('CONTROL_PRODUCCION_204', [periodo])
            cursor.callproc('CONTROL_PRODUCCION_205', [periodo])
            cursor.close() 
    json_data = json.dumps({'response': 'ok'})
    return HttpResponse(json_data, content_type='application/json')


def import_data_db_production(request):
    context = []
    context_header = []
    context_tipoPeriodo = []
    context_sector = []
    dictionary = my_dictionary()
    if request.method == 'POST':
        file_in_memory = request.FILES['import_file'].read()
        wb = load_workbook(filename=BytesIO(
            file_in_memory), data_only=True)
        ws = wb.active
        model = request.POST['model']
        data = ImportFile.objects.all().filter(name_model=model)
        context_tipoPeriodo = PeriodType.objects.all()
        first_row = 1
        for x in data:
            first_row = x.first_row
            data = Homologation.objects.all().filter(
                import_file_id=x.id).order_by('entry_header')
            for target_list in range(first_row, ws.max_row):
                dictionary = my_dictionary()
                for i in data:
                    if target_list == first_row:
                        context_header.append({
                            'header': i.db_header
                        })
                    dictionary.add(i.entry_header, ws.cell(
                        row=target_list, column=i.entry_header + 1).value)
                context.append(dictionary)
    return render(request, 'app_imports/bd_produccion_result.html', {'object_list': context, 'header': context_header, 'tipoPeriodo': context_tipoPeriodo, 'sector': context_sector})


def save_import_bd_produccion(request):
    if request.POST and request.is_ajax():
        count = 1
        json_obj = json.loads(request.POST.get('table'))
        for x in json_obj:
            try:
                if BDProduccion.objects.filter(period_id=request.POST['period'], code=x['Cod. SAP'], date=x['Fecha'], plant=x['Planta']).exists():
                    delete = BDProduccion.objects.filter(
                        period_id=request.POST['period'], code=x['Cod. SAP'], date=x['Fecha'], plant=x['Planta']).delete()
                model = BDProduccion()
                model.period_id = request.POST['period']
                model.code = x['Cod. SAP']
                model.description = x['Descripción']
                model.date = x['Fecha']
                model.k_producer = float(
                    x['Kg a producir'].replace(",", "."))
                model.k_real = float(x['Kg reales'].replace(",", "."))
                model.plant = x['Planta']
                model.productive_cap = float(
                    x['Tope Productivo'].replace(",", "."))
                model.sector_id = int(x['Sector'])
                model.save()
                count += 1
            except Exception as e:
                json_data = json.dumps({'error': str(e) + ', line: ' + str(count)})
                return HttpResponse(json_data, content_type='application/json')
    json_data = json.dumps({'response': 'ok'})
    return HttpResponse(json_data, content_type='application/json')


def import_data_stock_homologado(request):
    context = []
    context_header = []
    context_tipoPeriodo = []
    context_sector = []
    dictionary = my_dictionary()
    if request.method == 'POST':
        file_in_memory = request.FILES['import_file'].read()
        wb = load_workbook(filename=BytesIO(
            file_in_memory), data_only=True)
        ws = wb.active
        model = request.POST['model']
        data = ImportFile.objects.all().filter(name_model=model)
        context_tipoPeriodo = PeriodType.objects.all()
        first_row = 1
        for x in data:
            first_row = x.first_row
            data = Homologation.objects.all().filter(
                import_file_id=x.id).order_by('entry_header')
            for target_list in range(first_row, ws.max_row):
                dictionary = my_dictionary()
                for i in data:
                    if target_list == first_row:
                        context_header.append({
                            'header': i.db_header
                        })
                    dictionary.add(i.entry_header, ws.cell(
                        row=target_list, column=i.entry_header + 1).value)
                context.append(dictionary)
    return render(request, 'app_imports/stock_homologado_result.html', {'object_list': context, 'header': context_header, 'tipoPeriodo': context_tipoPeriodo, 'sector': context_sector})


def save_import_stock_homologado(request):
    if request.POST and request.is_ajax():
        count = 1
        json_obj = json.loads(request.POST.get('table'))
        for x in json_obj:
            try:
                if StockHomologado.objects.filter(period_id=request.POST.get('period'), code=x['Cod. SAP'], center=x['Centro'], destiny=x['Destino']).exists():
                    delete = StockHomologado.objects.filter(
                        period_id=request.POST.get('period'), code=x['Cod. SAP'], center=x['Centro'], destiny = x['Destino']).delete()
                free_stock = x['Stock Libre'].replace(",", ".")
                blocked_stock = x['Stock Bloqueado'].replace(",", ".")
                model = StockHomologado()
                model.period_id = request.POST.get('period')
                model.sector_id = int(x['Sector'])
                model.level2 = x['Nivel 2']
                model.code = x['Cod. SAP']
                model.description = x['Descripción']
                model.supplying_center = x['Centro Suministrador']
                model.center = x['Centro']
                model.destiny = x['Destino']
                model.free_stock = free_stock
                model.blocked_stock = blocked_stock
                model.save()
                count += 1
            except Exception as e:
                json_data = json.dumps({'error': str(e) + ', line: ' + str(count)})
                return HttpResponse(json_data, content_type='application/json')
        cur = connection.cursor()
        cur.callproc('CONTROL_STOCK_25', [request.POST['period']])
        cur.callproc('CONTROL_STOCK_26', [request.POST['period'],'MUESTRA', 'TODOS'])
        cur.callproc('CONTROL_STOCK_27', [request.POST['period'],'MUESTRA', 'TODOS'])
        cur.callproc('CONTROL_STOCK_28', [request.POST['period']])
        cur.close()
    json_data = json.dumps({'response': 'ok'})
    return HttpResponse(json_data, content_type='application/json')


def import_data_stock_meta(request):
    context = []
    context_header = []
    context_tipoPeriodo = []
    dictionary = my_dictionary()
    if request.method == 'POST':
        file_in_memory = request.FILES['import_file'].read()
        wb = load_workbook(filename=BytesIO(
            file_in_memory), data_only=True)
        ws = wb.worksheets[0]
        model = request.POST['model']
        data = ImportFile.objects.all().filter(name_model=model)
        context_tipoPeriodo = PeriodType.objects.all()
        first_row = 1
        for x in data:
            first_row = x.first_row
            data = Homologation.objects.all().filter(
                import_file_id=x.id).order_by('entry_header')
            for target_list in range(first_row, ws.max_row):
                dictionary = my_dictionary()
                for i in data:
                    if target_list == first_row:
                        context_header.append({
                            'header': i.db_header
                        })
                    dictionary.add(i.entry_header, ws.cell(
                        row=target_list, column=i.entry_header + 1).value)
                context.append(dictionary)
    return render(request, 'app_imports/stock_meta_result.html', {'object_list': context, 'header': context_header, 'tipoPeriodo': context_tipoPeriodo})


def save_import_stock_meta(request):
    if request.POST and request.is_ajax():
        count = 1
        json_obj = json.loads(request.POST.get('table'))
        for x in json_obj:
            try:
                if StockMeta.objects.filter(period_id=request.POST['period'], code=x['Cod. SAP']).exists():
                    delete = StockMeta.objects.filter(
                        period_id=request.POST['period'], code=x['Cod. SAP']).delete()
                model = StockMeta()
                model.period_id = request.POST['period']
                model.code = x['Cod. SAP']
                model.description = x['Descripción']
                model.stock = float(x['Stock Meta'].replace(",", "."))
                model.sector = x['Sector']
                model.save()
                count += 1
            except Exception as e:
                json_data = json.dumps({'error': str(e) + ', line: ' + str(count)})
                return HttpResponse(json_data, content_type='application/json')
        cur = connection.cursor()
        cur.callproc('CONTROL_STOCK_25', [request.POST['period'],'MUESTRA', 'TODOS'])
        cur.callproc('CONTROL_STOCK_26', [request.POST['period'],'MUESTRA', 'TODOS'])
        cur.callproc('CONTROL_STOCK_27', [request.POST['period'],'MUESTRA', 'TODOS'])
        cur.callproc('CONTROL_STOCK_28', [request.POST['period'],'MUESTRA', 'TODOS'])
        cur.close()
    json_data = json.dumps({'response': 'ok'})
    return HttpResponse(json_data, content_type='application/json')


def import_data_stock_insumos(request):
    context = []
    context_header = []
    context_tipoPeriodo = []
    context_sector = []
    dictionary = my_dictionary()
    if request.method == 'POST':
        file_in_memory = request.FILES['import_file'].read()
        wb = load_workbook(filename=BytesIO(
            file_in_memory), data_only=True)
        ws = wb.worksheets[0]
        model = request.POST['model']
        data = ImportFile.objects.all().filter(name_model=model)
        context_tipoPeriodo = PeriodType.objects.all()
        context_sector = Sector.objects.all()
        first_row = 1
        for x in data:
            first_row = x.first_row
            data = Homologation.objects.all().filter(
                import_file_id=x.id).order_by('entry_header')
            for target_list in range(first_row, ws.max_row):
                dictionary = my_dictionary()
                for i in data:
                    if target_list == first_row:
                        context_header.append({
                            'header': i.db_header
                        })
                    dictionary.add(i.entry_header, ws.cell(
                        row=target_list, column=i.entry_header + 1).value)
                context.append(dictionary)
    return render(request, 'app_imports/stock_insumos_result.html', {'object_list': context, 'header': context_header, 'tipoPeriodo': context_tipoPeriodo, 'sector': context_sector})


def save_import_stock_insumos(request):
    if request.POST and request.is_ajax():
        count = 1
        json_obj = json.loads(request.POST.get('table'))
        for x in json_obj:
            try:
                if StockInsumos.objects.filter(period_id=request.POST['period'], code=x['Cod. SAP'], center=x['Centro']).exists():
                    delete = StockInsumos.objects.filter(
                        period_id=request.POST['period'], code=x['Cod. SAP'], center=x['Centro']).delete()
                stock = x['Stock'].replace(".", "")
                stock = stock.replace(",", ".")
                free = x['Valor libre util.'].replace(".", "")
                free = free.replace(",", ".")
                model = StockInsumos()
                model.period_id = request.POST['period']
                model.center = x['Centro']
                model.code = x['Cod. SAP']
                model.description = x['Descripción']
                model.stock = float(stock)
                model.umb = x['UMB']
                model.free = float(free)
                model.almacen = x['Almacén']
                model.group = x['Gpo.artíc.']
                model.save()
                count += 1
            except Exception as e:
                json_data = json.dumps({'error': str(e) + ', line: ' + str(count)})
        json_data = json.dumps({'response': 'ok'})
    return HttpResponse(json_data, content_type='application/json')


def import_data_consumo_ead(request):
    context = []
    context_header = []
    context_tipoPeriodo = []
    dictionary = my_dictionary()
    if request.method == 'POST':
        file_in_memory = request.FILES['import_file'].read()
        wb = load_workbook(filename=BytesIO(
            file_in_memory), data_only=True)
        ws = wb.worksheets[0]
        model = request.POST['model']
        data = ImportFile.objects.all().filter(name_model=model)
        context_tipoPeriodo = PeriodType.objects.all()
        first_row = 1
        for x in data:
            first_row = x.first_row
            data = Homologation.objects.all().filter(
                import_file_id=x.id).order_by('entry_header')
            for target_list in range(first_row, ws.max_row):
                dictionary = my_dictionary()
                for i in data:
                    if target_list == first_row:
                        context_header.append({
                            'header': i.db_header
                        })
                    dictionary.add(i.entry_header, ws.cell(
                        row=target_list, column=i.entry_header + 1).value)
                context.append(dictionary)
    return render(request, 'app_imports/consumo_ead_result.html', {'object_list': context, 'header': context_header, 'tipoPeriodo': context_tipoPeriodo})


def save_import_consumo_ead(request):
    if request.POST and request.is_ajax():
        count = 1
        json_obj = json.loads(request.POST.get('table'))
        for x in json_obj:
            try:
                if ConsumoEAD.objects.filter(period_id=request.POST['period'], code=x['Cod. SAP'], plant_id=x['Centro'], reserve=x['Nro reserva']).exists():
                    delete = ConsumoEAD.objects.filter(
                        period_id=request.POST['period'], code=x['Cod. SAP'], plant_id=x['Centro'], reserve=x['Nro reserva']).delete()
                model = ConsumoEAD()
                model.period_id = request.POST['period']
                model.hour = x['Hora de entrada']
                model.classmov = x['Clase de movimiento']
                model.reserve = x['Nro reserva']
                model.document = x['Documento material']
                model.plant_id = x['Centro']
                model.name = x['Nombre 1']
                model.almacen = x['Almacén']
                model.lote = x['Lote']
                model.code = x['Cod. SAP']
                model.description = x['Descripción']
                model.date = x['Fecha']
                model.quantity_um = float(
                    x['Ctd.en UM entrada'].replace(",", "."))
                model.um = x['UM']
                model.quantity = float(x['Cantidad'].replace(",", "."))
                model.um_base = x['UM Base']
                model.save()
                count += 1
            except Exception as e:
                json_data = json.dumps({'error': str(e) + ', line: ' + str(count)})
        json_data = json.dumps({'response': 'ok'})
    return HttpResponse(json_data, content_type='application/json')


def import_data_reunion_volumen(request):
    context = []
    context_header = []
    context_tipoPeriodo = []
    context_sector = []
    dictionary = my_dictionary()
    if request.method == 'POST':
        file_in_memory = request.FILES['import_file'].read()
        wb = load_workbook(filename=BytesIO(
            file_in_memory), data_only=True)
        ws = wb.worksheets[0]
        model = request.POST['model']
        data = ImportFile.objects.all().filter(name_model=model)
        context_tipoPeriodo = PeriodType.objects.all()
        context_sector = Sector.objects.all()
        first_row = 1
        for x in data:
            first_row = x.first_row
            data = Homologation.objects.all().filter(
                import_file_id=x.id).order_by('entry_header')
            for target_list in range(first_row, ws.max_row):
                if ws.cell(row=target_list, column=5).value:
                    dictionary = my_dictionary()
                    for i in data:
                        if target_list == first_row:
                            context_header.append({
                                'header': i.db_header
                            })
                        dictionary.add(i.entry_header, ws.cell(
                            row=target_list, column=i.entry_header + 1).value)
                    context.append(dictionary)
    return render(request, 'app_imports/reunion_volumen_result.html', {'object_list': context, 'header': context_header, 'tipoPeriodo': context_tipoPeriodo, 'sector': context_sector})


def save_import_reunion_volumen(request):
    if request.POST and request.is_ajax():
        count = 1
        json_obj = json.loads(request.POST.get('table'))
        for x in json_obj:
            try:
                if ReunionVolumen.objects.filter(period_id=request.POST['period'], code=x['Cod. SAP'], destiny=x['Destino']).exists():
                    delete = ReunionVolumen.objects.filter(
                        period_id=request.POST['period'], code=x['Cod. SAP'], destiny=x['Destino']).delete()
                model = ReunionVolumen()
                model.period_id = request.POST['period']
                model.sector_id = request.POST['sector']
                model.code = x['Cod. SAP']
                model.description = x['Descripción']
                model.destiny = x['Destino']
                model.production = float(
                    x['Producción'].replace(",", "."))
                model.save()
                count += 1
            except Exception as e:
                json_data = json.dumps({'error': str(e) + ', line: ' + str(count)})
        json_data = json.dumps({'response': 'ok'})
    return HttpResponse(json_data, content_type='application/json')


class my_dictionary(dict):

    def __init__(self):
        self = dict()

    def add(self, key, value):
        self[key] = value


def test(request):
    periodos = Chart.objects.values('history_count').filter(id=61)
    for y in periodos:
        print(y['history_count'])
        periodo = 188 - y['history_count']
        print(periodo)
        cur = connection.cursor()
        cur.callproc('CONTROL_STOCK_61', [periodo - 1, 188,'MUESTRA', 'TODOS'])
        cur.close()
