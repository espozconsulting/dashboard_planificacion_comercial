from django.db import models
from apps.app_masterData.models import *


class Indicator(models.Model):
    name = models.CharField(verbose_name="Indicador", max_length=150)

    def __str__(self):
        return self.name


class TypeChart(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class DashStyle(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class MarkerSymbol(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class SizeChart(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Chart(models.Model):
    indicator = models.ForeignKey(
        Indicator, on_delete=models.CASCADE, verbose_name="Indicador")
    name = models.CharField(verbose_name="Nombre", max_length=150)
    history_count = models.IntegerField(
        verbose_name="Cant. historial", default=1)
    type_chart = models.ForeignKey(
        TypeChart, on_delete=models.CASCADE, verbose_name="Tipo de gráfico")
    dash_style = models.ForeignKey(
        DashStyle, on_delete=models.CASCADE, verbose_name="Tipo de línea", null=True, blank=True)
    marker_symbol = models.ForeignKey(
        MarkerSymbol, on_delete=models.CASCADE, verbose_name="Tipo de marcador", null=True, blank=True)
    size = models.ForeignKey(
        SizeChart, on_delete=models.CASCADE, verbose_name="Tamaño de gráfico", null=True, blank=True)
    title = models.CharField(verbose_name="Título",
                             max_length=50, null=True, blank=True)
    subtitle = models.CharField(
        verbose_name="Sub título", max_length=50, null=True, blank=True)
    xaxis_title = models.CharField(
        verbose_name="Título eje horizontal", max_length=50, null=True, blank=True)
    yaxis_title = models.CharField(
        verbose_name="Título eje vertical", max_length=50, null=True, blank=True)
    suffix = models.CharField(
        verbose_name="Sufijo", max_length=50, null=True, blank=True)
    show_suffix = models.BooleanField(
        verbose_name="Mostrar sufijo", default=False, null=True, blank=True)

    def __str__(self):
        return self.name


class ChartData(models.Model):
    chart = models.ForeignKey(
        Chart, verbose_name="Gráfico", on_delete=models.CASCADE)
    period = models.ForeignKey(
        Period, verbose_name="Periodo", on_delete=models.CASCADE)
    categories = models.CharField(verbose_name="Categorías", max_length=150)
    value = models.FloatField(default=0, verbose_name="Valor")
    serie = models.CharField(verbose_name="Nombre serie", max_length=150)
    filter_type = models.CharField(
        verbose_name="Tipo de filtro", max_length=50, null=True, blank=True)
    filter_name = models.CharField(
        verbose_name="Filtro", max_length=150, null=True, blank=True)


class Dashboard(models.Model):
    name = models.CharField(max_length=100)


class DashboardCharts(models.Model):
    dashboard = models.ForeignKey(Dashboard, on_delete=models.CASCADE)
    chart = models.ForeignKey(Chart, on_delete=models.CASCADE)
    gs_x = models.IntegerField(default=0)
    gs_y = models.IntegerField(default=0)
    show = models.BooleanField(default=False)
