from django.apps import AppConfig


class AppMasterdataConfig(AppConfig):
    name = 'app_masterData'
