from django import forms
from .models import *


class ImportFileForm(forms.ModelForm):

    class Meta:
        model = ImportFile
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ImportFileForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['first_row'].widget.attrs['class'] = 'form-control'
        self.fields['name_model'].widget.attrs['class'] = 'form-control'


class HomologationForm(forms.ModelForm):

    class Meta:
        model = Homologation
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(HomologationForm, self).__init__(*args, **kwargs)
        self.fields['import_file'].widget.attrs['class'] = 'form-control'
        self.fields['db_header'].widget.attrs['class'] = 'form-control'
        self.fields['entry_header'].widget.attrs['class'] = 'form-control'
