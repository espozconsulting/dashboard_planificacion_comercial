from django.contrib import admin
from django.urls import path, include

from apps.app_settings.views import *
from apps.app_auth.views import *
from apps.app_imports.views import *
from apps.app_masterData.views import *
from apps.app_kpi.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', HomeView.as_view(), name="home"),
    path('tablero/', DashboardView.as_view(), name="dashboard"),
    path('app_auth/listUsers', UsersListView.as_view(), name="userslist"),
    path('app_auth/listUsers/create', UserView.as_view(), name="userscreate"),
    path('app_auth/listUsers/update/<int:pk>/',
         UserEditView.as_view(), name="usersedit"),

    path('app_imports/production_program_list',
         ProductionProgramListView.as_view(), name="production_program_list"),
    path('app_imports/production_program_list/create',
         ProductionProgramImport.as_view(), name="production_program_create"),
    path('app_imports/production_program_list/result',
         import_data_production_program, name="import_data_production_program"),

    path('app_imports/db_production_list',
         DBProductionListView.as_view(), name="db_production_list"),
    path('app_imports/db_production_list/create',
         DBProductionImport.as_view(), name="db_production_create"),
    path('app_imports/db_production_list/result',
         import_data_db_production, name="import_data_db_production"),

    path('app_imports/stock_homologado_list',
         StockHomologadoListView.as_view(), name="stock_homologado_list"),
    path('app_imports/stock_homologado_list/create',
         StockHomologadoImport.as_view(), name="stock_homologado_create"),
    path('app_imports/stock_homologado_list/result',
         import_data_stock_homologado, name="import_data_stock_homologado"),

    path('app_imports/stock_meta_list',
         StockMetaListView.as_view(), name="stock_meta_list"),
    path('app_imports/stock_meta_list/create',
         StockMetaImport.as_view(), name="stock_meta_create"),
    path('app_imports/stock_meta_list/result',
         import_data_stock_meta, name="import_data_stock_meta"),

    path('app_imports/stock_insumos_list',
         StockInsumosListView.as_view(), name="stock_insumos_list"),
    path('app_imports/stock_insumos_list/create',
         StockInsumosImport.as_view(), name="stock_insumos_create"),
    path('app_imports/stock_insumos_list/result',
         import_data_stock_insumos, name="import_data_stock_insumos"),

    path('app_imports/consumo_list',
         ConsumoEADListView.as_view(), name="consumo_list"),
    path('app_imports/consumo_list/create',
         ConsumoEADImport.as_view(), name="consumo_create"),
    path('app_imports/consumo_list/result',
         import_data_consumo_ead, name="import_data_consumo"),

    path('app_imports/reunion_volumen_list',
         ReunionVolumenListView.as_view(), name="reunion_volumen_list"),
    path('app_imports/reunion_volumen_list/create',
         ReunionVolumenImport.as_view(), name="reunion_volumen_create"),
    path('app_imports/reunion_volumen_list/result',
         import_data_reunion_volumen, name="import_data_reunion_volumen"),

    path('app_kpi/indicator_list',
         IndicatorListView.as_view(), name="indicator_list"),
    path('app_kpi/indicator_list/create',
         IndicatorCreateView.as_view(), name="indicator_create"),
    path('app_kpi/indicator_list/update/<str:pk>/',
         IndicatorUpdateView.as_view(), name="indicator_edit"),

    path('app_kpi/chart_list',
         ChartListView.as_view(), name="chart_list"),
    path('app_kpi/chart_list/create',
         ChartCreateView.as_view(), name="chart_create"),
    path('app_kpi/chart_list/update/<str:pk>/',
         ChartUpdateView.as_view(), name="chart_edit"),

    path('app_settings/import_file_list',
         ImportFileListView.as_view(), name="import_file_list"),
    path('app_settings/import_file_list/create',
         ImportFileCreateView.as_view(), name="import_file_create"),
    path('app_settings/import_file_list/update/<int:pk>',
         ImportFileEditView.as_view(), name="import_file_edit"),

    path('app_settings/homologation_list',
         HomologationListView.as_view(), name="homologation_list"),
    path('app_settings/homologation_list/create',
         HomologationCreateView.as_view(), name="homologation_create"),
    path('app_settings/homologation_list/update/<int:pk>',
         HomologationEditView.as_view(), name="homologation_edit"),

    path('app_master/period_type_list',
         PeriodTypeListView.as_view(), name="period_type_list"),
    path('app_master/period_type_list/create',
         PeriodTypeCreateView.as_view(), name="period_type_create"),
    path('app_master/period_type_list/update/<int:pk>/',
         PeriodTypeUpdateView.as_view(), name="period_type_edit"),

    path('app_master/period_list',
         PeriodListView.as_view(), name="period_list"),
    path('app_master/period_list/create',
         PeriodCreateView.as_view(), name="period_create"),
    path('app_master/period_list/update/<int:pk>/',
         PeriodUpdateView.as_view(), name="period_edit"),

    path('app_master/sector_list',
         SectorListView.as_view(), name="sector_list"),
    path('app_master/sector_list/create',
         SectorCreateView.as_view(), name="sector_create"),
    path('app_master/sector_list/update/<str:pk>/',
         SectorUpdateView.as_view(), name="sector_edit"),



    path('ajax/get_columns/', get_columns),
    path('ajax/get_periods/', get_periods),
    path('ajax/get_chart/', get_chart),
    path('ajax/get_charts/', get_charts),

    path('ajax/save_homologation/', save_homologation),
    path('ajax/save_import_programa_produccion/',
         save_import_programa_produccion),
    path('ajax/save_import_bd_produccion/',
         save_import_bd_produccion),
    path('ajax/save_import_stock_homologado/',
         save_import_stock_homologado),
    path('ajax/save_import_stock_meta/',
         save_import_stock_meta),
    path('ajax/save_import_stock_insumos/',
         save_import_stock_insumos),
    path('ajax/save_import_consumo_ead/',
         save_import_consumo_ead),
    path('ajax/save_import_reunion_volumen/',
         save_import_reunion_volumen),


    path('ajax/delete_chart/', delete_chart),
    path('ajax/delete_sector/', delete_sector),


    path('ajax/create_chart', create_chart),
    path('ajax/create_chart_tablero', create_chart_tablero),
    path('ajax/create_chart_tablero/before', create_chart_tablero_before),
    path('ajax/create_chart_tablero/after', create_chart_tablero_after),
    path('ajax/chart_dashboard', chart_dashboard),

    path('test', test),

]
