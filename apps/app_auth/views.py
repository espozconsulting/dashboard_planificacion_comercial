from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin

from .forms import *


class UsersListView(LoginRequiredMixin, ListView):
    model = User
    template_name = "app_auth/list.html"


class UserView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    model = User
    form_class = SignUpForm
    template_name = "app_auth/edit.html"
    success_url = "/app_auth/listUsers"
    success_message = "%(username)s ha sido creado satisfactoriamente"


class UserEditView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = User
    form_class = UpdateUserForm
    template_name = "app_auth/edit.html"
    success_url = "/app_auth/listUsers"
    success_message = "%(username)s ha sido actualizado satisfactoriamente"
