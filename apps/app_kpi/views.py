from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponse
from django.db import connection
from django.db.models import Sum

import json

from .models import *
from .forms import *


class IndicatorListView(LoginRequiredMixin, ListView):
    model = Indicator
    template_name = "app_kpi/list_indicator.html"


class IndicatorCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    model = Indicator
    form_class = IndicatorForm
    template_name = "app_kpi/edit_indicator.html"
    success_url = "/app_kpi/indicator_list"
    success_message = "%(name)s ha sido creado satisfactoriamente"


class IndicatorUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Indicator
    form_class = IndicatorForm
    template_name = "app_kpi/edit_indicator.html"
    success_url = "/app_kpi/indicator_list"
    success_message = "%(name)s ha sido actualizado satisfactoriamente"


class ChartListView(LoginRequiredMixin, ListView):
    model = Chart
    template_name = "app_kpi/list_chart.html"


class ChartCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    model = Chart
    form_class = ChartForm
    template_name = "app_kpi/edit_chart.html"
    success_url = "/app_kpi/chart_list"
    success_message = "%(name)s ha sido creado satisfactoriamente"


class ChartUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Chart
    form_class = ChartForm
    template_name = "app_kpi/edit_chart.html"
    success_url = "/app_kpi/chart_list"
    success_message = "%(name)s ha sido actualizado satisfactoriamente"


def get_chart(request):
    if request.POST:
        context = []
        charts = Chart.objects.values('id', 'size__name').filter(
            id=request.POST['chart_id'])
        for res in charts:
            context.append({
                'chart_id': res['id'],
                'size': res['size__name']
            })
        json_data = json.dumps({'response': context})
    return HttpResponse(json_data, content_type='application/json')


def get_charts(request):
    if request.POST:
        context = []
        charts = Chart.objects.values('id', 'size__name').filter(
            indicator_id=request.POST['indicator_id']).order_by('id')
        for res in charts:
            context.append({
                'chart_id': res['id'],
                'size': res['size__name']
            })
        json_data = json.dumps({'response': context})
    return HttpResponse(json_data, content_type='application/json')


def create_chart(request):
    chart = Chart.objects.values(
        'id', 'type_chart__name', 'title', 'subtitle', 'xaxis_title', 'yaxis_title', 'dash_style__name', 'marker_symbol__name', 'suffix', 'show_suffix', 'history_count').filter(id=request.POST['chart_id'])
    type_chart = ""
    title = ""
    subtitle = ""
    xaxis_title = ""
    yaxis_title = ""
    dash_style = ""
    marker_symbol = ""
    serie_name = ""
    suffix = ""
    show_suffix = ""
    serie = ""
    category = ""
    chart_dashboard = False
    chart_id = 0
    period_id = 0
    period_from = 0
    history_count = 0
    count_category = 0
    categories = []
    serie_value = []
    series = []
    values = []
    for chart in chart:
        chart_id = chart['id']
        if chart['type_chart__name'] == 'column with cumpl':
            type_chart = 'column'
        else:
            type_chart = chart['type_chart__name']
        title = chart['title']
        subtitle = chart['subtitle']
        xaxis_title = chart['xaxis_title']
        yaxis_title = chart['yaxis_title']
        dash_style = chart['dash_style__name']
        marker_symbol = chart['marker_symbol__name']
        suffix = chart['suffix']
        show_suffix = chart['show_suffix']
        history_count = chart['history_count']
    chart_period = ChartData.objects.values('period_id').filter(
        chart_id=request.POST['chart_id']).order_by('-id')[:1]
    for x in chart_period:
        period_id = x['period_id']
        period_from = period_id - (history_count - 1)
    chart_data = ChartData.objects.values('serie', 'period_id').filter(
        chart_id=request.POST['chart_id'], period_id=period_id).annotate(Sum('value')).order_by('-serie')
    for y in chart_data:
        categories = []
        values = []
        if history_count > 1:
            series_data = ChartData.objects.all().filter(
                chart_id=request.POST['chart_id'], serie=y['serie'], filter_name='TODOS', filter_type='MUESTRA', period_id__lte=period_id).order_by('id')[:history_count]
        else:
            series_data = ChartData.objects.all().filter(
                chart_id=request.POST['chart_id'], serie=y['serie'], filter_name='TODOS', filter_type='MUESTRA', period_id=period_id).order_by('id')
        count_category = 0
        for x in series_data:
            category = x.categories
            if count_category == 0:
                categories.append(category.replace(" 00:00:00", ""))
            if type_chart == 'pie':
                values.append({
                    'name': x.categories,
                    'y': x.value
                })
            else:
                values.append(x.value)
            serie = x.serie
            count_category += count_category
        if chart['type_chart__name'] == 'column with cumpl':
            if y['serie'] == '% Cumpl':
                series.append({
                    'name': serie,
                    'data': values,
                    'type': 'line',
                    'dashStyle': dash_style,
                    'yAxis': 1,
                    'marker': {
                        'symbol': marker_symbol
                    }
                })
            else:
                series.append({
                    'name': serie,
                    'data': values,
                    'type': 'column',
                    'dashStyle': dash_style,
                    'marker': {
                        'symbol': marker_symbol
                    }
                })
        else:
            series.append({
                'name': serie,
                'data': values,
                'dashStyle': dash_style,
                'marker': {
                    'symbol': marker_symbol
                }
            })
    if DashboardCharts.objects.all().filter(chart_id=chart_id, dashboard_id=5, show=True).exists():
        chart_dashboard = True
    json_data = json.dumps({
        'chart_id': chart_id,
        'type_chart': type_chart,
        'title': title,
        'subtitle': subtitle,
        'xaxis_title': xaxis_title,
        'yaxis_title': yaxis_title,
        'categories': categories,
        'series': series,
        'suffix': suffix,
        'show_suffix': show_suffix,
        'history': history_count,
        'chart_dashboard': chart_dashboard
    })
    return HttpResponse(json_data, content_type='application/json')


def create_chart_tablero(request):
    chart = Chart.objects.values(
        'id', 'type_chart__name', 'title', 'subtitle', 'xaxis_title', 'yaxis_title', 'dash_style__name', 'marker_symbol__name', 'suffix', 'show_suffix', 'history_count').filter(id=request.POST['chart_id'])
    type_chart = ""
    title = ""
    subtitle = ""
    xaxis_title = ""
    yaxis_title = ""
    dash_style = ""
    marker_symbol = ""
    serie_name = ""
    suffix = ""
    show_suffix = ""
    serie = ""
    category = ""
    chart_dashboard = False
    chart_id = 0
    period_id = 0
    period_from = 0
    history_count = 0
    count_category = 0
    categories = []
    serie_value = []
    series = []
    values = []
    for chart in chart:
        chart_id = chart['id']
        if chart['type_chart__name'] == 'column with cumpl':
            type_chart = 'column'
        else:
            type_chart = chart['type_chart__name']
        title = chart['title']
        subtitle = chart['subtitle']
        xaxis_title = chart['xaxis_title']
        yaxis_title = chart['yaxis_title']
        dash_style = chart['dash_style__name']
        marker_symbol = chart['marker_symbol__name']
        suffix = chart['suffix']
        show_suffix = chart['show_suffix']
        history_count = chart['history_count']
    chart_period = ChartData.objects.values('period_id').filter(
        chart_id=request.POST['chart_id'], filter_name=request.POST['sector_id']).order_by('-id')[:1]
    for x in chart_period:
        period_id = x['period_id']
    chart_data = ChartData.objects.values('serie', 'period_id').filter(
        chart_id=request.POST['chart_id'], filter_name=request.POST['sector_id'], period_id=period_id).annotate(Sum('value')).order_by('-serie')
    for y in chart_data:
        categories = []
        values = []
        if history_count > 1:
            series_data = ChartData.objects.all().filter(
                chart_id=request.POST['chart_id'], serie=y['serie'], filter_name=request.POST['sector_id'], filter_type='SECTOR').order_by('-id')[:history_count]
            series_data = reversed(series_data)
        else:
            if subtitle == "":
                period = Period.objects.all().filter(id=period_id)
                for new_period in period:
                    subtitle = new_period.name
            series_data = ChartData.objects.all().filter(
                chart_id=request.POST['chart_id'], serie=y['serie'], filter_name=request.POST['sector_id'], filter_type='SECTOR', period_id=period_id).order_by('id')
        count_category = 0
        for x in series_data:
            category = x.categories
            if count_category == 0:
                categories.append(category.replace(" 00:00:00", ""))
            if type_chart == 'pie':
                values.append({
                    'name': x.categories,
                    'y': x.value
                })
            else:
                values.append(x.value)
            serie = x.serie
            count_category += count_category
        if chart['type_chart__name'] == 'column with cumpl':
            if y['serie'] == '% Cumpl':
                series.append({
                    'name': serie,
                    'data': values,
                    'type': 'line',
                    'dashStyle': dash_style,
                    'yAxis': 2,
                    'marker': {
                        'symbol': marker_symbol
                    }
                })
            else:
                series.append({
                    'name': serie,
                    'data': values,
                    'type': 'column',
                    'dashStyle': dash_style,
                    'marker': {
                        'symbol': marker_symbol
                    }
                })
        else:
            series.append({
                'name': serie,
                'data': values,
                'dashStyle': dash_style,
                'marker': {
                    'symbol': marker_symbol
                }
            })
    if DashboardCharts.objects.all().filter(chart_id=chart_id, dashboard_id=5, show=True).exists():
        chart_dashboard = True
    json_data = json.dumps({
        'chart_id': chart_id,
        'type_chart': type_chart,
        'title': title,
        'subtitle': subtitle,
        'xaxis_title': xaxis_title,
        'yaxis_title': yaxis_title,
        'categories': categories,
        'series': series,
        'suffix': suffix,
        'show_suffix': show_suffix,
        'history': history_count,
        'chart_dashboard': chart_dashboard,
        'period_id': period_id
    })
    return HttpResponse(json_data, content_type='application/json')


def chart_dashboard(request):
    if request.POST:
        show = False
        if request.POST['checked'] == 'true':
            show = True
        if DashboardCharts.objects.all().filter(chart_id=request.POST['chart_id'], dashboard_id=5).exists():
            dash = DashboardCharts.objects.filter(
                chart_id=request.POST['chart_id'], dashboard_id=5).update(show=show)
        else:
            dash = DashboardCharts()
            dash.chart_id = request.POST['chart_id']
            dash.dashboard_id = 5
            dash.order = 0
            dash.show = show
            dash.save()
        json_data = json.dumps({'response': 'ok'})
    return HttpResponse(json_data, content_type='application/json')


def delete_chart(request):
    if request.POST:
        chart = Chart.objects.filter(id=request.POST['chart_id']).delete()
        json_data = json.dumps({'response': 'ok'})
    return HttpResponse(json_data, content_type='application/json')


def delete_sector(request):
    if request.POST:
        chart = Sector.objects.filter(code=request.POST['sector_id']).delete()
        json_data = json.dumps({'response': 'ok'})
    return HttpResponse(json_data, content_type='application/json')


def create_chart_tablero_before(request):
    chart = Chart.objects.values(
        'id', 'type_chart__name', 'title', 'subtitle', 'xaxis_title', 'yaxis_title', 'dash_style__name', 'marker_symbol__name', 'suffix', 'show_suffix', 'history_count').filter(id=request.POST['chart_id'])
    type_chart = ""
    title = ""
    subtitle = ""
    xaxis_title = ""
    yaxis_title = ""
    dash_style = ""
    marker_symbol = ""
    serie_name = ""
    suffix = ""
    show_suffix = ""
    serie = ""
    category = ""
    chart_dashboard = False
    chart_id = 0
    period_id = 0
    period_from = 0
    history_count = 0
    count_category = 0
    categories = []
    serie_value = []
    series = []
    values = []
    for chart in chart:
        chart_id = chart['id']
        if chart['type_chart__name'] == 'column with cumpl':
            type_chart = 'column'
        else:
            type_chart = chart['type_chart__name']
        title = chart['title']
        subtitle = chart['subtitle']
        xaxis_title = chart['xaxis_title']
        yaxis_title = chart['yaxis_title']
        dash_style = chart['dash_style__name']
        marker_symbol = chart['marker_symbol__name']
        suffix = chart['suffix']
        show_suffix = chart['show_suffix']
        history_count = chart['history_count']
    chart_data = ChartData.objects.values('serie', 'period_id').filter(
        chart_id=request.POST['chart_id'], filter_name=request.POST['sector_id'], period_id__lt=request.POST['period_id']).annotate(Sum('value')).order_by('-serie')[:1]
    for y in chart_data:
        period_id = y['period_id']
        categories = []
        values = []
        if history_count > 1:
            series_data = ChartData.objects.all().filter(
                chart_id=request.POST['chart_id'], serie=y['serie'], filter_name=request.POST['sector_id'], filter_type='SECTOR', period_id__lt=request.POST['period_id']).order_by('-id')[:history_count]
            series_data = reversed(series_data)
        else:
            if subtitle == "":
                period = Period.objects.all().filter(id=y['period_id'])
                for new_period in period:
                    subtitle = new_period.name
            series_data = ChartData.objects.all().filter(
                chart_id=request.POST['chart_id'], serie=y['serie'], filter_name=request.POST['sector_id'], filter_type='SECTOR', period_id=y['period_id']).order_by('id')
        count_category = 0
        for x in series_data:
            category = x.categories
            if count_category == 0:
                categories.append(category.replace(" 00:00:00", ""))
            if type_chart == 'pie':
                values.append({
                    'name': x.categories,
                    'y': x.value
                })
            else:
                values.append(x.value)
            serie = x.serie
            count_category += count_category
        if chart['type_chart__name'] == 'column with cumpl':
            if y['serie'] == '% Cumpl':
                series.append({
                    'name': serie,
                    'data': values,
                    'type': 'line',
                    'dashStyle': dash_style,
                    'yAxis': 2,
                    'marker': {
                        'symbol': marker_symbol
                    }
                })
            else:
                series.append({
                    'name': serie,
                    'data': values,
                    'type': 'column',
                    'dashStyle': dash_style,
                    'marker': {
                        'symbol': marker_symbol
                    }
                })
        else:
            series.append({
                'name': serie,
                'data': values,
                'dashStyle': dash_style,
                'marker': {
                    'symbol': marker_symbol
                }
            })
    if DashboardCharts.objects.all().filter(chart_id=chart_id, dashboard_id=5, show=True).exists():
        chart_dashboard = True
    json_data = json.dumps({
        'chart_id': chart_id,
        'type_chart': type_chart,
        'title': title,
        'subtitle': subtitle,
        'xaxis_title': xaxis_title,
        'yaxis_title': yaxis_title,
        'categories': categories,
        'series': series,
        'suffix': suffix,
        'show_suffix': show_suffix,
        'history': history_count,
        'chart_dashboard': chart_dashboard,
        'period_id': period_id
    })
    return HttpResponse(json_data, content_type='application/json')


def create_chart_tablero_after(request):
    chart = Chart.objects.values(
        'id', 'type_chart__name', 'title', 'subtitle', 'xaxis_title', 'yaxis_title', 'dash_style__name', 'marker_symbol__name', 'suffix', 'show_suffix', 'history_count').filter(id=request.POST['chart_id'])
    type_chart = ""
    title = ""
    subtitle = ""
    xaxis_title = ""
    yaxis_title = ""
    dash_style = ""
    marker_symbol = ""
    serie_name = ""
    suffix = ""
    show_suffix = ""
    serie = ""
    category = ""
    chart_dashboard = False
    chart_id = 0
    period_id = 0
    period_from = 0
    history_count = 0
    count_category = 0
    categories = []
    serie_value = []
    series = []
    values = []
    for chart in chart:
        chart_id = chart['id']
        if chart['type_chart__name'] == 'column with cumpl':
            type_chart = 'column'
        else:
            type_chart = chart['type_chart__name']
        title = chart['title']
        subtitle = chart['subtitle']
        xaxis_title = chart['xaxis_title']
        yaxis_title = chart['yaxis_title']
        dash_style = chart['dash_style__name']
        marker_symbol = chart['marker_symbol__name']
        suffix = chart['suffix']
        show_suffix = chart['show_suffix']
        history_count = chart['history_count']
    chart_data = ChartData.objects.values('serie', 'period_id').filter(
        chart_id=request.POST['chart_id'], filter_name=request.POST['sector_id'], period_id__gt=request.POST['period_id']).annotate(Sum('value')).order_by('-serie')[:1]
    for y in chart_data:
        period_id = y['period_id']
        categories = []
        values = []
        if history_count > 1:
            series_data = ChartData.objects.all().filter(
                chart_id=request.POST['chart_id'], serie=y['serie'], filter_name=request.POST['sector_id'], filter_type='SECTOR', period_id__lte=period_id).order_by('-id')[:history_count]
            series_data = reversed(series_data)
        else:
            if subtitle == "":
                period = Period.objects.all().filter(id=y['period_id'])
                for new_period in period:
                    subtitle = new_period.name
            series_data = ChartData.objects.all().filter(
                chart_id=request.POST['chart_id'], serie=y['serie'], filter_name=request.POST['sector_id'], filter_type='SECTOR', period_id=y['period_id']).order_by('id')
        count_category = 0
        for x in series_data:
            category = x.categories
            if count_category == 0:
                categories.append(category.replace(" 00:00:00", ""))
            if type_chart == 'pie':
                values.append({
                    'name': x.categories,
                    'y': x.value
                })
            else:
                values.append(x.value)
            serie = x.serie
            count_category += count_category
        if chart['type_chart__name'] == 'column with cumpl':
            if y['serie'] == '% Cumpl':
                series.append({
                    'name': serie,
                    'data': values,
                    'type': 'line',
                    'dashStyle': dash_style,
                    'yAxis': 2,
                    'marker': {
                        'symbol': marker_symbol
                    }
                })
            else:
                series.append({
                    'name': serie,
                    'data': values,
                    'type': 'column',
                    'dashStyle': dash_style,
                    'marker': {
                        'symbol': marker_symbol
                    }
                })
        else:
            series.append({
                'name': serie,
                'data': values,
                'dashStyle': dash_style,
                'marker': {
                    'symbol': marker_symbol
                }
            })
    if DashboardCharts.objects.all().filter(chart_id=chart_id, dashboard_id=5, show=True).exists():
        chart_dashboard = True
    json_data = json.dumps({
        'chart_id': chart_id,
        'type_chart': type_chart,
        'title': title,
        'subtitle': subtitle,
        'xaxis_title': xaxis_title,
        'yaxis_title': yaxis_title,
        'categories': categories,
        'series': series,
        'suffix': suffix,
        'show_suffix': show_suffix,
        'history': history_count,
        'chart_dashboard': chart_dashboard,
        'period_id': period_id
    })
    return HttpResponse(json_data, content_type='application/json')
