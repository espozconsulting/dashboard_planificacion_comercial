from django import forms
from .models import *


class IndicatorForm(forms.ModelForm):

    class Meta:
        model = Indicator
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(IndicatorForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'


class ChartForm(forms.ModelForm):

    class Meta:
        model = Chart
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ChartForm, self).__init__(*args, **kwargs)
        self.fields['indicator'].widget.attrs['class'] = 'form-control'
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['history_count'].widget.attrs['class'] = 'form-control'
        self.fields['type_chart'].widget.attrs['class'] = 'form-control'
        self.fields['size'].widget.attrs['class'] = 'form-control'
        self.fields['dash_style'].widget.attrs['class'] = 'form-control'
        self.fields['marker_symbol'].widget.attrs['class'] = 'form-control'
        self.fields['title'].widget.attrs['class'] = 'form-control'
        self.fields['subtitle'].widget.attrs['class'] = 'form-control'
        self.fields['xaxis_title'].widget.attrs['class'] = 'form-control'
        self.fields['yaxis_title'].widget.attrs['class'] = 'form-control'
        self.fields['suffix'].widget.attrs['class'] = 'form-control'
        self.fields['show_suffix'].widget.attrs['class'] = 'form-control'
