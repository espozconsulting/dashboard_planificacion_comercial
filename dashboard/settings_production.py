import os
from dashboard.settings import *

DEBUG = False
TEMPLATE_DEBUG = False
ALLOWED_HOSTS = ['*']
SECRET_KEY = 'umpp&#l%k$9war7^5f33h&+fgq=v^i#r_+w!h+#b8vo#zq0329'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
